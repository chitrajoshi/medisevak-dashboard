const FormValidation = (field, value) => {
    switch (field) {
        case "name": {
            if (!value) {
                return false;
            }
            return true;
        }
        case "email": {
            if (!value) {
                return false;
            } else if (!isValidEmail(value)) {
                return false;
            }
            return true;
        }
        case "mobile": {
            if (!value) {
                return false;
            }
            return true;
        }
        case "img": {
            const imageFile = value;
            let validationObject = { status: false, msg: '' };
            if (!imageFile) {
                validationObject.msg = 'No file uploaded';
                return validationObject;
            }
            const fileType = imageFile.type;
            if (fileType !== 'image/png' && fileType !== 'image/jpeg' && fileType !== 'image/jpg') {
                validationObject.msg = 'File format not correct';
                return validationObject;
            }
            if (fileType > 5120) {
                validationObject.msg = "Only files less than 5mb are permitted to be uploaded";
                return validationObject;
            }
            validationObject.msg = '';
            validationObject.status = true;
            return validationObject;
        }
    }
}

const isValidEmail = (value) => {
    // const filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    const filter = /^([a-zA-Z0-9_])+(([a-zA-Z0-9])+\.)+([a-zA-Z0-9]{2,4})+$/;
    const isValid = filter.test(value);
    return isValid;
}

export default FormValidation;