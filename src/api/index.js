import * as config from '../constants/app-config';
import configs from './fetch-configs';
// import { compose } from '../../../../../Library/Caches/typescript/3.0/node_modules/redux';

export const request = (method, path, payload, isMultipart) => {
  // console.log(method, path, payload, isMultipart);
  const newPromise = new Promise((resolve, reject) => {
    _request(method, path, payload, isMultipart, (err, response) => {
      if (err) {
        return reject(err);
      }
      return resolve(response);
    })
  });
  return newPromise;
}

async function _request(method, path, payload, isMultipart, cb) {
  const serverPath = `${config.SERVER_URL}${path}`;
  
  // console.log("method: " + method, "serverPath: " + serverPath , "payload: " + payload);

  const fetchConfigs = configs(method, payload, isMultipart);
  try {
    const response = await fetch(serverPath, fetchConfigs);
    if (!response.ok) {
      if (response.status == 404) {
        return cb(new Error("Request api not found"));
      }
    }
    return cb(null, response.json());
  } catch (err) {
    return cb(err);
  }
}
