import lService from '../services/ls-service';
const ls = lService();
import * as appConfigs from '../constants/app-config';

const fetchConfig = (method, load, isMultipart, token) => {
  // console.log(method, load, isMultipart, token);
  const url = `${appConfigs.SERVER_URL}`;
  const headers = new Headers();
  if (isMultipart) {
    // headers.append('Content-Type', 'multipart/form-data');
  } else {
    headers.append('Content-Type', 'application/json');
  }
  //some new text added want to add more, need more testing
  headers.append('Accept', 'application/json');
  headers.append('Access-Control-Allow-Origin', url);

  let authToken = ls.get(appConfigs.TOKEN_NAME);
  if (token) {
    authToken = token;
  }
  if (authToken) {
    headers.append(appConfigs.TOKEN_NAME, authToken);
  }

  const config = {
    method: method,
    credentials: 'omit',
    headers: headers
  };
  if (method === "post" || method === "put") {
    if (!isMultipart) {
      const payload = JSON.stringify(load);
      config.body = payload;
    } else {
      let payload = new FormData();
      Object.keys(load).forEach((key) => {
        payload.append(key, load[key]);
      });
      config.body = payload;
    }
    // console.log("form-data :" , config.body);

  }

  return Object.assign({}, config);
};

export default fetchConfig;
