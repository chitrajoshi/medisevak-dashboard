const generateRandomNumber = (random) => {
  const randomNumber = Math.floor(Math.random() * 100000 * 10 * random);
  const currentTime = Date.now();
  return randomNumber + currentTime;
};

export default generateRandomNumber;