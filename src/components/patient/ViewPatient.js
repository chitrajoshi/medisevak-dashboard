import React from 'react';
import './patient.scss';
import * as actions from '../../actions/patientActions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import queryString from 'query-string';
import { SERVER_URL } from '../../constants/app-config';

class ViewPatient extends React.Component {

    componentDidMount() {
        const id = queryString.parse(this.props.location.search).id;
        this.props.actions.get_patient_by_id(id);
    }

    mapPatientValues = (feature, index) => {
        return (
            <div key={index} className="feature-body">
                <div className="view-doctor-value" >{feature}</div>
            </div>
        );
    }

    render() {
        const patient = this.props.patient;
        let imageElement;
        if(patient.patient_pic_url.value){
            imageElement = <img src={SERVER_URL + '/' + patient.patient_pic_url.value} />
        }else{
            imageElement = <img src={require('../../assets/images/dummyPatient.jpg')}/>
        }
        return (
            <div className="doctor">
                <h2>View Patient</h2>

                <form>
                    <h3>Basic Info</h3>
                    <div className="basic-info">
                        <div className="info">
                            <label>Name</label>
                            <div className="view-doctor-value">{patient.name.value}</div>
                            <label>Email</label>
                            <div className="view-doctor-value">{patient.email.value}</div>
                            <label>Mobile</label>
                            <div className="view-doctor-value">{patient.mobile.value}</div>
                        </div>
                        <div className="img">
                            {imageElement}
                            <div className="upload-img-btn">{patient.name.value}</div>
                        </div>
                    </div>
                </form>

                <form>
                    <h3>Other Info</h3>
                    <div className="other-info">
                        <label>Gender</label>
                        <div className="view-doctor-value">{patient.gender.value}</div>
                        <label>DOB</label>
                        <div className="view-doctor-value">{patient.dob.value}</div>
                    </div>
                </form>

                {/* <form>
                    <div className="other">
                        <div className="field-heading">
                            <label>Treatments</label>
                        </div>
                        {doctor.treatments.value.map(this.mapDoctorValues)}
                    </div>
                </form> */}
    
            </div>
        );
    }
}

ViewPatient.propTypes = {
    patient: PropTypes.object,
    location: PropTypes.object,
    actions: PropTypes.object
}

const mapStateToProps = (state) => {
    return {
        patient: state.patient
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(actions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ViewPatient);