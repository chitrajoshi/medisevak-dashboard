import React from 'react';
import './patient.scss';
import * as actions from '../../actions/patientActions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import queryString from 'query-string';
// import DoctorFeature from './DoctorFeature';
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { SERVER_URL } from '../../constants/app-config';

class EditPatient extends React.Component {

    constructor(props) {
        super(props);
        this.currentImage ='';
        this.img='';
    }

    componentDidMount() {
        const id = queryString.parse(this.props.location.search).id;
        this.props.actions.get_patient_by_id(id);
    }

    onhandleChange = (event) => {
        const id = queryString.parse(this.props.location.search).id;

        const field = event.target.getAttribute('data-name');
        const value = event.target.value;

        this.props.actions.handle_edit_patient_change({ id: id, field: field, value: value });
    }

    imageUploadHandler = (event) => {
        const files = event.currentTarget.files;
        const file = files[0];
        if(file){
            this.img = file;
            this.currentImage = URL.createObjectURL(file);
        }
        this.props.actions.image_upload_handler(file);
    }

    onhandleSubmit = () => {
        let obj = {};
        obj.id = this.props.patient.id;
        for (var key in this.props.patient) {
            if (this.props.patient[key].edited) {
                if(key === 'patient_pic_url'){
                    obj[key] = this.img
                }else{
                    obj[key] = this.props.patient[key].value
                }
            }
        }
       
        this.props.actions.handle_edit_patient_submit(obj);
    }

    addFieldValues = (event) => {
        const id = queryString.parse(this.props.location.search).id;

        const fieldName = event.currentTarget.getAttribute('data-name');
        const field = fieldName + '-' + this.props.patient[fieldName].value.length;
        const value = '';

        this.props.actions.handle_edit_patient_change({ id: id, field: field, value: value });
    }

    deleteFieldlValue = (event) => {
        const id = queryString.parse(this.props.location.search).id;

        const field = event.currentTarget.getAttribute('data-name');
        const value = undefined;

        this.props.actions.handle_edit_patient_change({ id: id, field: field, value: value });
    }

    // mapTreatment = (treatment, index) => {
    //     return (
    //         <div className="feature-body" key={index}>
    //             <input
    //                 type="text"
    //                 placeholder="Treatment"
    //                 data-name={"treatments-" + index}
    //                 onChange={this.onhandleChange}
    //                 value={treatment} />
    //             <div className="cancel" >
    //                 <span onClick={this.deleteFieldlValue} data-name={"treatments-" + index} >
    //                     <FontAwesomeIcon icon={faTimes} />
    //                 </span>
    //             </div>
    //         </div>
    //     );
    // }

    render() {
        const patient = this.props.patient;
        let imageElement;
        if(this.currentImage){
            imageElement = <img src={this.currentImage}/>
        }else if(patient.patient_pic_url.value){
            imageElement = <img src={SERVER_URL + '/' + patient.patient_pic_url.value} />
        }else{
            imageElement = <img src={require('../../assets/images/dummyPatient.jpg')}/>
        }

        return (
            <div className="doctor">
                <h2>Edit Patient</h2>

                <form>
                    <h3>Basic Info</h3>
                    <div className="basic-info">
                        <div className="info">

                            <label>Name</label>
                            <input
                                type="text"
                                placeholder="Name"
                                data-name='name'
                                value={patient.name.value || ''}
                                onChange={this.onhandleChange}
                                readOnly />

                            <label>Email</label>
                            <input
                                type="email"
                                placeholder="Email"
                                data-name='email'
                                value={patient.email.value || ''}
                                onChange={this.onhandleChange}
                                readOnly />

                            <label>Mobile</label>
                            <input
                                type="text"
                                placeholder="Mobile"
                                data-name='mobile'
                                value={patient.mobile.value || ''}
                                onChange={this.onhandleChange} 
                                readOnly/>
                        </div>

                        <div className="img">                        
                        {imageElement}
                            <div className="upload-img-btn">
                                EDIT
                                <input
                                    type="file"
                                    className="file-input"
                                    data-name="img"
                                    onChange={this.imageUploadHandler} />
                            </div>
                        </div>

                    </div>
                </form>

                <form>
                    <h3>Other Info</h3>
                    <div className="other-info">
                        <label>Gender</label>
                        <input
                            type="text"
                            placeholder="Gender"
                            data-name="gender"
                            onChange={this.onhandleChange}
                            value={patient.gender.value || ''} />

                        <label>DOB</label>
                        <input
                            type="text"
                            placeholder="DOB"
                            data-name="dob"
                            onChange={this.onhandleChange}
                            value={patient.dob.value || ''} />

                    </div>

                </form>

                {/* <DoctorFeature
                    labelName="Treatments"
                    dataName="treatments"
                    doctor={doctor.treatments}
                    addFieldValues={this.addFieldValues}
                    mapFeatureValues={this.mapTreatment} /> */}


                <div className="doctor-btn">
                    <span onClick={this.onhandleSubmit}>SAVE</span>
                </div>

            </div>
        );
    }
}

EditPatient.propTypes = {
    patient: PropTypes.object,
    location: PropTypes.object,
    actions: PropTypes.object
}

const mapStateToProps = (state) => {
    return {
        patient: state.patient
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(actions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditPatient);