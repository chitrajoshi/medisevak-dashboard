import React from 'react';
import * as actions from '../../actions/patientActions';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import PropTypes from 'prop-types';
import './patient.scss';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEdit, faTrash, faFile, faSearch } from '@fortawesome/free-solid-svg-icons'


class PatientList extends React.Component {

    componentDidMount = () => {
        this.props.actions.get_all_patients();
    }

    onhandleViewPatientClick = (event) => {
        event.stopPropagation();
        const id = event.currentTarget.getAttribute("data-id");
        this.props.actions.on_handle_view_patient(id);
    }

    onhandleAddPatientClick = () => {
        this.props.actions.on_handle_add_patient();
    }

    onhandleEditPatientClick = (event) => {
        event.stopPropagation();
        const id = event.currentTarget.getAttribute("data-id");
        this.props.actions.on_handle_edit_patient(id);
    }

    onhandleDeletePatientClick = (event) => {
        event.stopPropagation();
        const id = event.currentTarget.getAttribute("data-id");
        this.props.actions.on_handle_delete_patient(id);
    }

    onhandleSearchChange = (event) => {
        const searchString = event.currentTarget.value;
        this.props.actions.on_handle_Search(searchString);
    }

    mapPatientRow = (patient, index) => {
        return (
            <li key={index} data-id={patient._id}>
                <div className="body">{index + 1}</div>
                <div className="body">{patient.name}</div>
                <div className="body">{patient.treatment}</div>
                <div className="body">{patient.location}</div>
                <div className="body" onClick={this.onhandleViewPatientClick} data-id={patient._id}><FontAwesomeIcon className="icons" icon={faFile} /></div>
                <div className="body" onClick={this.onhandleEditPatientClick} data-id={patient._id}><FontAwesomeIcon className="icons" icon={faEdit} /></div>
                <div className="body" onClick={this.onhandleDeletePatientClick} data-id={patient._id}><FontAwesomeIcon className="icons" icon={faTrash} /></div>
            </li>
        );
    }

    render() {

        return (
            <div className="doctor-listing">
                <h2>Patients</h2>

                <form>
                    <div className="doctor-top">
                        <div className="search">
                            <input 
                                type="text"  
                                placeholder="Search Patient..."
                                onChange ={this.onhandleSearchChange}
                                data-name ="Search"
                                 />
                            <span><FontAwesomeIcon className="icons" icon={faSearch} /> </span>
                        </div>
                        <div className="add-doctor" onClick={this.onhandleAddPatientClick}>Add Patient</div>
                    </div>

                    <div className="line"></div>

                    <div className="listing-heading">
                        <div className="heading">S.No</div>
                        <div className="heading">Name</div>
                        <div className="heading">Treatment</div>
                        <div className="heading">City</div>
                        <div className="heading">View</div>
                        <div className="heading">Edit</div>
                        <div className="heading">Delete</div>
                    </div>

                    <ul className="listing-body">
                        {this.props.patients.map(this.mapPatientRow)}
                    </ul>
                </form>
            </div>
        );
    }
}

PatientList.propTypes = {
    patients: PropTypes.array,
    actions: PropTypes.object
}

const mapStateToProps = (state) => {
    return {
        patients: state.patientList.filteredPatients
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(actions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PatientList);