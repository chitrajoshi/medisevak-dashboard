import React from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import PropTypes from 'prop-types';
import './login.scss';

import * as actions from '../../actions/loginActions';

class LoginPage extends React.Component {

    constructor(props){
        super(props);
        this.onChangeForm = this.onChangeForm.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    onChangeForm(event){
        const field = event.target.getAttribute("data-name");
        const value = event.target.value;
        this.props.actions.handle_login_form_change({field, value});
    }

    handleSubmit(){
        this.props.actions.loginForm(this.props.login)
    }

    render() {
        // if(state.login.loginStatus){
        //     return (<Link to="/doctors"></Link>);
        // }

        return (
            <div className="login-container">
                <div className="login-form">
                    <div className="login-title">
                    <img src={require('../../assets/images/logo.png')}/>
                    </div>
                    <form>

                        <label>Login Id</label>
                        <input 
                            type="text" 
                            placeholder="Enter your LoginId" 
                            value={this.props.login.loginId} 
                            onChange={this.onChangeForm}
                            data-name="mobile"
                             />

                        <label>Password</label>
                        <input 
                            type="password" 
                            placeholder="Enter your Password" 
                            value={this.props.login.password} 
                            onChange={this.onChangeForm}
                            data-name="password" />

                        <div className="login-btn" onClick={this.handleSubmit}>LOGIN</div>
                    </form>
                </div>
            </div>
        );
    }
}

LoginPage.propTypes = {
    login : PropTypes.object,
    actions : PropTypes.object
}


function mapStatetoProps(state){
    // console.log(state.login);
    
    return{
        login : state.login
    }
}

function mapDispatchToProps(dispatch){
    // console.log(dispatch);
    return{
        actions: bindActionCreators(actions, dispatch)
    }
}

export default connect(mapStatetoProps,
    mapDispatchToProps) (LoginPage);