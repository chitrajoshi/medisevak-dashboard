import React from 'react';
import { hot } from 'react-hot-loader';
import PropTypes from "prop-types";

import Toastr from '../components/common/toastr';
import PageLoader from '../components/common/loaders/pageLoader';
import Sidebar from '../components/common/sidebar/sidebar';
import Routes from '../components/Routes';

import '../styles/reset.css';
import '../styles/fonts.css';
import '../styles/root.css';
import '../styles/app.scss';

class App extends React.Component {
    render() {
        return (
            <div className="app">

                <div className="top-navbar">
                    <div className="logo"><img src={require('../assets/images/logo.png')} /></div>
                    <div className="logout">Logout</div>
                </div>

                <Sidebar />

                <div className="body-container">
                    <PageLoader />
                    <Toastr />
                    <Routes />
                </div>

            </div>
        );
    }
}

App.propTypes = {
    children: PropTypes.element
};

export default hot(module)(App);