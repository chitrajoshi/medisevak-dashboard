import React from 'react';
import * as actions from '../../actions/doctorActions';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import PropTypes from 'prop-types';
import './doctor.scss';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEdit, faTrash, faFile, faSearch } from '@fortawesome/free-solid-svg-icons'


class DoctorList extends React.Component {

    componentDidMount = () => {
        this.props.actions.get_all_doctors();
    }

    onhandleViewDoctorClick = (event) => {
        event.stopPropagation();
        const id = event.currentTarget.getAttribute("data-id");
        this.props.actions.on_handle_view_doctor(id);
    }

    onhandleAddDoctorClick = () => {
        this.props.actions.on_handle_add_doctor();
    }

    onhandleEditDoctorClick = (event) => {
        event.stopPropagation();
        const id = event.currentTarget.getAttribute("data-id");
        this.props.actions.on_handle_edit_doctor(id);
    }

    onhandleDeleteDoctorClick = (event) => {
        event.stopPropagation();
        const id = event.currentTarget.getAttribute("data-id");
        this.props.actions.on_handle_delete_doctor(id);
    }

    onhandleSearchChange = (event) => {
        const searchString = event.currentTarget.value;
        this.props.actions.on_handle_Search(searchString);
    }

    mapDoctorRow = (doctor, index) => {
        return (
            <li key={index} data-id={doctor._id}>
                <div className="body">{index + 1}</div>
                <div className="body">{doctor.name}</div>
                <div className="body">{doctor.treatment}</div>
                <div className="body">{doctor.location}</div>
                <div className="body" onClick={this.onhandleViewDoctorClick} data-id={doctor._id}><FontAwesomeIcon className="icons" icon={faFile} /></div>
                <div className="body" onClick={this.onhandleEditDoctorClick} data-id={doctor._id}><FontAwesomeIcon className="icons" icon={faEdit} /></div>
                <div className="body" onClick={this.onhandleDeleteDoctorClick} data-id={doctor._id}><FontAwesomeIcon className="icons" icon={faTrash} /></div>
            </li>
        );
    }

    render() {

        return (
            <div className="doctor-listing">
                <h2>Doctors</h2>

                <form>
                    <div className="doctor-top">
                        <div className="search">
                            <input 
                                type="text"  
                                placeholder="Search Doctor..."
                                onChange ={this.onhandleSearchChange}
                                data-name ="Search"
                                 />
                            <span><FontAwesomeIcon className="icons" icon={faSearch} /> </span>
                        </div>
                        <div className="add-doctor" onClick={this.onhandleAddDoctorClick}>Add Doctor</div>
                    </div>

                    <div className="line"></div>

                    <div className="listing-heading">
                        <div className="heading">S.No</div>
                        <div className="heading">Name</div>
                        <div className="heading">Treatment</div>
                        <div className="heading">City</div>
                        <div className="heading">View</div>
                        <div className="heading">Edit</div>
                        <div className="heading">Delete</div>
                    </div>

                    <ul className="listing-body">
                        {this.props.doctors.map(this.mapDoctorRow)}
                    </ul>
                </form>
            </div>
        );
    }
}

DoctorList.propTypes = {
    doctors: PropTypes.array,
    actions: PropTypes.object
}

const mapStateToProps = (state) => {
    return {
        doctors: state.doctorList.filteredDoctors
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(actions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DoctorList);