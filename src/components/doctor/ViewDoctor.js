import React from 'react';
import './doctor.scss';
import * as actions from '../../actions/doctorActions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import queryString from 'query-string';
import { SERVER_URL } from '../../constants/app-config';

class ViewDoctor extends React.Component {
    componentDidMount() {
        const id = queryString.parse(this.props.location.search).id;
        this.props.actions.get_doctor_by_id(id);
    }

    mapDoctorValues = (feature, index) => {
        return (
            <div key={index} className="feature-body">
                <div className="view-doctor-value" >{feature}</div>
            </div>
        );
    }

    render() {
        const doctor = this.props.doctor;
        let imageElement;
        if(doctor.profile_pic_url.value){
            imageElement = <img src={SERVER_URL + '/' + doctor.profile_pic_url.value} />
        }else{
            imageElement = <img src={require('../../assets/images/dummyDoctor.jpg')}/>
        }
        return (
            <div className="doctor">
                <h2>View Doctor</h2>

                <form>
                    <h3>Basic Info</h3>
                    <div className="basic-info">
                        <div className="info">
                            <label>Name</label>
                            <div className="view-doctor-value">{doctor.name.value}</div>
                            <label>About</label>
                            <div className="view-doctor-value">{doctor.description.value}</div>
                            <label>Email</label>
                            <div className="view-doctor-value">{doctor.email.value}</div>
                            <label>Mobile</label>
                            <div className="view-doctor-value">{doctor.mobile.value}</div>
                        </div>
                        <div className="img">
                            {imageElement}
                            <div className="upload-img-btn">{doctor.name.value}</div>
                        </div>
                    </div>
                </form>

                <form>
                    <h3>Other Info</h3>
                    <div className="other-info">
                        <label>Experience Years</label>
                        <div className="view-doctor-value">{doctor.experience_years.value}</div>
                        <label>Avg. Fees</label>
                        <div className="view-doctor-value">{doctor.avg_fees.value}</div>
                    </div>
                </form>

                <form>
                    <div className="other">
                        <div className="field-heading">
                            <label>Treatments</label>
                        </div>
                        {doctor.treatments.value.map(this.mapDoctorValues)}
                    </div>
                </form>

                <form>
                    <div className="other">
                        <div className="field-heading">
                            <label>Specializations</label>
                        </div>
                        {doctor.specializations.value.map(this.mapDoctorValues)}
                    </div>
                </form>

                <form>
                    <div className="other">
                        <div className="field-heading">
                            <label>Qualifications</label>
                        </div>
                        {doctor.qualifications.value.map(this.mapDoctorValues)}
                    </div>
                </form>

                <form>
                    <div className="other">
                        <div className="field-heading">
                            <label>Work Experiences</label>
                        </div>
                        {doctor.work_experiences.value.map(this.mapDoctorValues)}
                    </div>
                </form>

                <form>
                    <div className="other">
                        <div className="field-heading">
                            <label>Awards</label>
                        </div>
                        {doctor.awards.value.map(this.mapDoctorValues)}
                    </div>
                </form>

                <form>
                    <div className="other">
                        <div className="field-heading">
                            <label>Hospitals</label>
                        </div>
                        {doctor.hospital_list.value.map(this.mapDoctorValues)}
                    </div>
                </form>
    
            </div>
        );
    }
}

ViewDoctor.propTypes = {
    doctor: PropTypes.object,
    location: PropTypes.object,
    actions: PropTypes.object
}

const mapStateToProps = (state) => {
    return {
        doctor: state.doctor
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(actions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ViewDoctor);