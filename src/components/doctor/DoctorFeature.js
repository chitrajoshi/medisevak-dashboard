import React from 'react';
import PropTypes from 'prop-types';

const DoctorFeature = ({labelName, dataName, doctor, addFieldValues, mapFeatureValues}) => {
    return (
        <form>
            <div className="other">
                <div className="field-heading">
                    <label>{labelName}</label>
                    <div className="add-field-value">
                        <span onClick={addFieldValues} data-name={dataName}>ADD</span>
                    </div>
                </div>
                {doctor.value.map(mapFeatureValues)}
            </div>
        </form>
    );
}

DoctorFeature.propTypes = {
    labelName: PropTypes.string,
    dataName: PropTypes.string,
    doctor: PropTypes.object,
    addFieldValues: PropTypes.func,
    mapFeatureValues: PropTypes.func
}

export default DoctorFeature;