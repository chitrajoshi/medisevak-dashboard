import React from 'react';
import './doctor.scss';
import * as actions from '../../actions/doctorActions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import DoctorFeature from './DoctorFeature';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'

class AddDoctor extends React.Component {

    constructor(props) {
        super(props);
        this.img = "";
        this.currentImage = "";
    }

    onhandleChange = (event) => {
        const field = event.target.getAttribute("data-name");
        const value = event.target.value;

        this.props.actions.handle_doctor_form_change({ field, value });
    }

    onClickSubmit = () => {
        event.preventDefault();
        this.props.actions.handle_doctor_form_submit(this.props.doctor, this.img);
    }

    imageUploadHandler = (event) => {
        const files = event.currentTarget.files;
        const file = files[0];
        this.img = file;
        if(file){
            this.currentImage = URL.createObjectURL(file);
            // console.log(this.currentImage);
        }
        this.props.actions.image_upload_handler(file);
    }

    addFieldValues = (event) => {
        const fieldName = event.currentTarget.getAttribute('data-name');
        const field = fieldName + '-' + this.props.doctor[fieldName].value.length;
        const value = '';

        this.props.actions.handle_doctor_form_change({ field, value });
    }

    deleteFieldlValue = (event) => {
        const field = event.currentTarget.getAttribute('data-name');
        const value = undefined;

        this.props.actions.handle_doctor_form_change({ field: field, value: value });
    }

    mapTreatment = (treatment, index) => {
        // console.log(treatment);
        return (
            <div className="feature-body" key={index}>
                <input
                    type="text"
                    placeholder="Treatment"
                    data-name={"treatments-" + index}
                    onChange={this.onhandleChange}
                    value={treatment} />
                <div className="cancel" >
                    <span onClick={this.deleteFieldlValue} data-name={"treatments-" + index}>
                        <FontAwesomeIcon icon={faTimes} />
                    </span>
                </div>
            </div>
        );
    }

    mapSpecializations = (specialization, index) => {
        return (
            <div className="feature-body" key={index}>
                <input
                    type="text"
                    placeholder="Specialization"
                    data-name={"specializations-" + index}
                    onChange={this.onhandleChange}
                    value={specialization} />
                <div className="cancel">
                    <span onClick={this.deleteFieldlValue} data-name={"specializations-" + index}>
                        <FontAwesomeIcon icon={faTimes} />
                    </span>
                </div>
            </div>
        );
    }

    mapQualifications = (qualification, index) => {
        return (
            <div className="feature-body" key={index}>
                <input
                    type="text"
                    placeholder="Qualification"
                    data-name={"qualifications-" + index}
                    onChange={this.onhandleChange}
                    value={qualification} />
                <div className="cancel">
                    <span onClick={this.deleteFieldlValue} data-name={"qualifications-" + index}>
                        <FontAwesomeIcon icon={faTimes} />
                    </span>
                </div>
            </div>
        );
    }

    mapWorkExperiences = (work_experience, index) => {
        return (
            <div className="feature-body" key={index}>
                <input
                    type="text"
                    placeholder="Work Experience"
                    data-name={"work_experiences-" + index}
                    onChange={this.onhandleChange}
                    value={work_experience} />
                <div className="cancel">
                    <span onClick={this.deleteFieldlValue} data-name={"work_experiences-" + index}>
                        <FontAwesomeIcon icon={faTimes} />
                    </span>
                </div>
            </div>
        );
    }

    mapAward = (award, index) => {
        return (
            <div className="feature-body" key={index}>
                <input
                    type="text"
                    placeholder="Award"
                    data-name={"awards-" + index}
                    onChange={this.onhandleChange}
                    value={award} />
                <div className="cancel">
                    <span onClick={this.deleteFieldlValue} data-name={"awards-" + index}>
                        <FontAwesomeIcon icon={faTimes} />
                    </span>
                </div>
            </div>
        );
    }

    mapHospitals = (hospital, index) => {
        return (
            <div className="feature-body" key={index}>
                <input
                    key={index}
                    type="text"
                    placeholder="Hospital"
                    data-name={"hospital_list-" + index}
                    onChange={this.onhandleChange}
                    value={hospital} />
                <div className="cancel">
                    <span onClick={this.deleteFieldlValue} data-name={"hospital_list-" + index}>
                        <FontAwesomeIcon icon={faTimes} />
                    </span>
                </div>
            </div>
        );
    }

    render() {
        const doctor = this.props.doctor;
        let imageElement;
        if(this.currentImage){
            imageElement = <img src={this.currentImage} />
        }else{
            imageElement = <img src={require('../../assets/images/dummyDoctor.jpg')}/>
        }
        
        return (
            <div className="doctor">
                <h2>Add Doctor</h2>

                <form>
                    <h3>Basic Info</h3>
                    <div className="basic-info">
                        <div className="info">
                            <label>Name<sup>*</sup></label>
                            <input
                                required
                                type="text"
                                placeholder="Name"
                                data-name="name"
                                onChange={this.onhandleChange}
                                value={doctor.name.value} />
                            {doctor.name.isError ? <div className="error">Please enter valid Name</div> : ''}

                            <label>About</label>
                            <input
                                type="text"
                                placeholder="About"
                                data-name="description"
                                onChange={this.onhandleChange}
                                value={doctor.description.value} />

                            <label>Email<sup>*</sup></label>
                            <input
                                required
                                type="email"
                                placeholder="Email"
                                data-name="email"
                                onChange={this.onhandleChange}
                                value={doctor.email.value} />
                            {doctor.email.isError ? <div className="error">Please enter valid Email</div> : ''}

                            <label>Mobile<sup>*</sup></label>
                            <input
                                required
                                type="text"
                                placeholder="Phone"
                                data-name="mobile"
                                onChange={this.onhandleChange}
                                value={doctor.mobile.value} />
                            {doctor.mobile.isError ? <div className="error">Please enter valid Mobile</div> : ''}

                        </div>
                        <div className="img">
                            {imageElement}
                            <div className="upload-img-btn">
                                UPLOAD
                                <input
                                    type="file"
                                    className="file-input"
                                    data-name="img"
                                    onChange={this.imageUploadHandler} />
                            </div>
                        </div>
                    </div>
                </form>

                <form>
                    <h3>Other Info</h3>

                    <div className="other-info">
                        <label>Experience Years</label>
                        <input
                            type="text"
                            placeholder="Experience"
                            data-name="experience_years"
                            onChange={this.onhandleChange}
                            value={doctor.experience_years.value} />

                        <label>Avg. Fees</label>
                        <input
                            type="text"
                            placeholder="fees"
                            data-name="avg_fees"
                            onChange={this.onhandleChange}
                            value={doctor.avg_fees.value} />
                    </div>
                </form>

                <DoctorFeature
                    labelName="Treatments"
                    dataName="treatments"
                    doctor={doctor.treatments}
                    addFieldValues={this.addFieldValues}
                    mapFeatureValues={this.mapTreatment}
                />

                <DoctorFeature
                    labelName="Specializations"
                    dataName="specializations"
                    doctor={doctor.specializations}
                    addFieldValues={this.addFieldValues}
                    mapFeatureValues={this.mapSpecializations}
                />

                <DoctorFeature
                    labelName="Qualifications"
                    dataName="qualifications"
                    doctor={doctor.qualifications}
                    addFieldValues={this.addFieldValues}
                    mapFeatureValues={this.mapQualifications}
                />

                <DoctorFeature
                    labelName="Work Experiences"
                    dataName="work_experiences"
                    doctor={doctor.work_experiences}
                    addFieldValues={this.addFieldValues}
                    mapFeatureValues={this.mapWorkExperiences}
                />

                <DoctorFeature
                    labelName="Awards"
                    dataName="awards"
                    doctor={doctor.awards}
                    addFieldValues={this.addFieldValues}
                    mapFeatureValues={this.mapAward}
                />

                <DoctorFeature
                    labelName="Hospitals"
                    dataName="hospital_list"
                    doctor={doctor.hospital_list}
                    addFieldValues={this.addFieldValues}
                    mapFeatureValues={this.mapHospitals}
                />

                <div className="doctor-btn">
                    <span onClick={this.onClickSubmit}>SUBMIT</span>
                </div>

            </div>
        );
    }
}

AddDoctor.propTypes = {
    doctor: PropTypes.object,
    actions: PropTypes.object
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(actions, dispatch)
    }
}

const mapStateToProps = (state) => {
    return {
        doctor: state.doctor
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddDoctor);