import React from 'react';
import { Route, Switch } from "react-router-dom";
import LoginPage from './login/LoginPage';

import DoctorList from './doctor/DoctorList';
import ViewDoctor from "./doctor/ViewDoctor";
import AddDoctor from './doctor/AddDoctor';
import EditDoctor from './doctor/EditDoctor';

import HospitalList from './hospital/HospitalList';
import ViewHospital from './hospital/ViewHospital';
import AddHospital from './hospital/AddHospital';
import EditHospital from './hospital/EditHospital';

import TreatmentList from './treatment/TreatmentList';

import PatientList from './patient/PatientList';
import ViewPatient from './patient/ViewPatient';
import AddPatient from './patient/AddPatient';
import EditPatient from './patient/EditPatient';

class Routes extends React.Component {
    render() {
        return (
            <Switch>
                <Route path="/login" component={LoginPage} />

                <Route exact path="/doctors" component={DoctorList} />
                <Route path="/viewDoctor" component={ViewDoctor} />
                <Route path="/addDoctor" component={AddDoctor} />
                <Route path="/editDoctor" component={EditDoctor} />

                <Route path="/hospitals" component={HospitalList}/>
                <Route path="/viewHospital" component={ViewHospital}/>
                <Route path="/addHospital" component={AddHospital}/>
                <Route path="/editHospital" component={EditHospital}/>

                <Route path="/treatments" component={TreatmentList}/>

                <Route path="/patients" component={PatientList}/>
                <Route path="/viewPatient" component={ViewPatient}/>
                <Route path="/addPatient" component={AddPatient}/>
                <Route path="/editPatient" component={EditPatient}/>
            </Switch>
        )
    }
}

export default Routes;