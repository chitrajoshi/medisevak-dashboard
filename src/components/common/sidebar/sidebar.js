import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../../../actions/routeActions/routeActions';
import PropTypes from 'prop-types';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUserMd, faHospital, faPlus, faUser } from '@fortawesome/free-solid-svg-icons'


class Sidebar extends React.Component {

    onhandleClick = (event) => {
        const selectedMenu = event.currentTarget.getAttribute("data-name");
        if (selectedMenu === 'doctor') {
            this.props.actions.go_to_doctors(selectedMenu);
        } else if (selectedMenu === "hospital") {
            this.props.actions.go_to_hospitals(selectedMenu);
        } else if (selectedMenu === "treatment") {
            this.props.actions.go_to_treatments(selectedMenu);
        } else {
            this.props.actions.go_to_patients(selectedMenu);
        }
    }

    render() {
        return (
            <div className="sidebar">
                <ul>
                    <li data-name="doctor" onClick={this.onhandleClick} className={this.props.sidebar === 'doctor' ? 'menu-selected' : 'menu'}>
                        <FontAwesomeIcon icon={faUserMd} />
                        <div className="name">Doctors</div>
                    </li>

                    <li data-name="hospital" onClick={this.onhandleClick} className={this.props.sidebar === 'hospital' ? 'menu-selected' : 'menu'}>
                        <FontAwesomeIcon icon={faHospital} />
                        <div className="name">Hospitals</div>
                    </li>
                    <li data-name="treatment" onClick={this.onhandleClick} className={this.props.sidebar === 'treatment' ? 'menu-selected' : 'menu'}>
                        <FontAwesomeIcon icon={faPlus} />
                        <div className="name">Treatments</div>
                    </li>
                    <li data-name="patient" onClick={this.onhandleClick} className={this.props.sidebar === 'patient' ? 'menu-selected' : 'menu'}>
                        <FontAwesomeIcon icon={faUser} />
                        <div className="name">Patients</div>
                    </li>
                </ul>
            </div>
        );
    }
}

Sidebar.propTypes = {
    actions: PropTypes.object,
    sidebar: PropTypes.string
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(actions, dispatch)
    }
}

const mapStateToProps = (state) => {
    return {
        sidebar: state.sideBar.selected
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar);

