import "./pageLoader.css";
import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

class PageLoader extends React.Component {
  render() {
    let elmClass = "loader-container";
    if (this.props.is_loading) {
      elmClass += " loading";
    }
    return (
      <div className="page-container">
        <div className={elmClass}>
          <div className="loader">
            <div className="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>            
            <p>{this.props.msg} Please wait </p>
          </div>
        </div>
      </div>
    );
  }
}
PageLoader.propTypes = {
  is_loading: PropTypes.bool.isRequired,
  msg: PropTypes.string
}

const mapStateToProps = (state) => {
  return {
    is_loading: state.pageLoader.is_loading,
    msg: state.pageLoader.msg
  }
}

export default connect(mapStateToProps)(PageLoader);
