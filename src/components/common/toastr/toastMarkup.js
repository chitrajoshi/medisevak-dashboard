import React from 'react';
import PropTypes from 'prop-types';
import './toastr.css';

const ToastMarkup = ({ toasts, removeToast }) => {

    const mapToasts = (toast) => {
        let toastClass = 'toast warning show';
        if(toast.status){
            toastClass = 'toast okay show';
        }
        const msg = JSON.stringify(toast.msg);
        return(
            <li className={toastClass} key={toast.id}>
                <button className="closeButton" data-id={toast.id} onClick={removeToast}>Close</button>
                <p className="msg">{msg}</p>
            </li>
        );
    }

    return(
        <ul className="toastr">
            {(toasts.length > 0) && toasts.map(mapToasts)}
        </ul>
    );

}

ToastMarkup.propTypes = {
    toasts : PropTypes.array.isRequired,
    removeToast : PropTypes.func.isRequired
}

export default ToastMarkup;