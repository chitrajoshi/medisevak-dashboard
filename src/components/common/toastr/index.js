import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import ToastMarkup from './toastMarkup';
import * as actions from '../../../actions/toastrActions';

class Toastr extends React.Component{
    constructor(){
        super();
        this.removeToast = this.removeToast.bind(this);
    }

    removeToast = (event) => {
        const toastId = event.currentTarget.getAttribute('data-id');
        this.props.actions.remove_toast({id: toastId});
    }

    render(){
        return(
            <ToastMarkup 
                toasts={this.props.toasts}
                removeToast={this.removeToast}/>
        );
    }
}

Toastr.propTypes = {
    toasts : PropTypes.array.isRequired,
    actions : PropTypes.object.isRequired
}

const mapStateToProps = (state) => {
    return {
        toasts : state.toasts
    }
}

const mapDisptachToProps = (dispatch) => {
    return {
        actions : bindActionCreators(actions, dispatch)
    }
}

export default connect (mapStateToProps,
    mapDisptachToProps) (Toastr); 