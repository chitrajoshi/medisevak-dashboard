import React from 'react';
import * as actions from '../../actions/hospitalActions';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import PropTypes from 'prop-types';
import './hospital.scss';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faEdit, faTrash, faFile, faSearch } from '@fortawesome/free-solid-svg-icons'


class HospitalList extends React.Component {

    componentDidMount = () => {
        this.props.actions.get_all_hospitals();
    }

    onhandleViewHospitalClick = (event) => {
        event.stopPropagation();
        const id = event.currentTarget.getAttribute("data-id");
        this.props.actions.on_handle_view_hospital(id);
    }

    onhandleAddHospitalClick = () => {
        this.props.actions.on_handle_add_hospital();
    }

    onhandleEditHospitalClick = (event) => {
        event.stopPropagation();
        const id = event.currentTarget.getAttribute("data-id");
        this.props.actions.on_handle_edit_hospital(id);
    }

    onhandleDeleteHospitalClick = (event) => {
        event.stopPropagation();
        const id = event.currentTarget.getAttribute("data-id");
        this.props.actions.on_handle_delete_hospital(id);
    }

    onhandleSearchChange = (event) => {
        const searchString = event.currentTarget.value;
        this.props.actions.on_handle_Search(searchString);
    }

    mapHospitalRow = (hospital, index) => {
        return (
            <li key={index} data-id={hospital._id}>
                <div className="body">{index + 1}</div>
                <div className="body">{hospital.name}</div>
                <div className="body">{hospital.treatment}</div>
                <div className="body">{hospital.location}</div>
                <div className="body" onClick={this.onhandleViewHospitalClick} data-id={hospital._id}><FontAwesomeIcon className="icons" icon={faFile} /></div>
                <div className="body" onClick={this.onhandleEditHospitalClick} data-id={hospital._id}><FontAwesomeIcon className="icons" icon={faEdit} /></div>
                <div className="body" onClick={this.onhandleDeleteHospitalClick} data-id={hospital._id}><FontAwesomeIcon className="icons" icon={faTrash} /></div>
            </li>
        );
    }

    render() {

        return (
            <div className="doctor-listing">
                <h2>Hospitals</h2>

                <form>
                    <div className="doctor-top">
                        <div className="search">
                            <input 
                                type="text"  
                                placeholder="Search Hospital..."
                                onChange ={this.onhandleSearchChange}
                                data-name ="Search"
                                 />
                            <span><FontAwesomeIcon className="icons" icon={faSearch} /> </span>
                        </div>
                        <div className="add-doctor" onClick={this.onhandleAddHospitalClick}>Add Hospital</div>
                    </div>

                    <div className="line"></div>

                    <div className="listing-heading">
                        <div className="heading">S.No</div>
                        <div className="heading">Name</div>
                        <div className="heading">Treatment</div>
                        <div className="heading">City</div>
                        <div className="heading">View</div>
                        <div className="heading">Edit</div>
                        <div className="heading">Delete</div>
                    </div>

                    <ul className="listing-body">
                        {this.props.hospitals.map(this.mapHospitalRow)}
                    </ul>
                </form>
            </div>
        );
    }
}

HospitalList.propTypes = {
    hospitals: PropTypes.array,
    actions: PropTypes.object
}

const mapStateToProps = (state) => {
    return {
        hospitals: state.hospitalList.filteredHospitals
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(actions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HospitalList);