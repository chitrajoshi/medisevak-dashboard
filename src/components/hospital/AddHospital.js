import React from 'react';
import './hospital.scss';
import * as actions from '../../actions/hospitalActions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import DoctorFeature from '../doctor/DoctorFeature';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'

class AddHospital extends React.Component {

    constructor(props) {
        super(props);
        this.img = "";
        this.currentImage = "";
    }

    onhandleChange = (event) => {
        const field = event.target.getAttribute("data-name");
        const value = event.target.value;

        this.props.actions.handle_hospital_form_change({ field, value });
    }

    onClickSubmit = () => {
        event.preventDefault();
        this.props.actions.handle_hospital_form_submit(this.props.hospital, this.img);
    }

    imageUploadHandler = (event) => {
        const files = event.currentTarget.files;
        const file = files[0];
        this.img = file;
        if(file){
            this.currentImage = URL.createObjectURL(file);
            // console.log(this.currentImage);
        }
        this.props.actions.image_upload_handler(file);
    }

    addFieldValues = (event) => {
        const fieldName = event.currentTarget.getAttribute('data-name');
        const field = fieldName + '-' + this.props.hospital[fieldName].value.length;
        const value = '';

        this.props.actions.handle_hospital_form_change({ field, value });
    }

    deleteFieldlValue = (event) => {
        const field = event.currentTarget.getAttribute('data-name');
        const value = undefined;

        this.props.actions.handle_hospital_form_change({ field: field, value: value });
    }

    mapTreatment = (treatment, index) => {
        // console.log(treatment);
        return (
            <div className="feature-body" key={index}>
                <input
                    type="text"
                    placeholder="Treatment"
                    data-name={"treatments-" + index}
                    onChange={this.onhandleChange}
                    value={treatment} />
                <div className="cancel" >
                    <span onClick={this.deleteFieldlValue} data-name={"treatments-" + index}>
                        <FontAwesomeIcon icon={faTimes} />
                    </span>
                </div>
            </div>
        );
    }

    render() {
        const hospital = this.props.hospital;
        let imageElement;
        if(this.currentImage){
            imageElement = <img src={this.currentImage} />
        }else{
            imageElement = <img src={require('../../assets/images/dummyHospital.png')}/>
        }
        
        return (
            <div className="doctor">
                <h2>Add Hospital</h2>

                <form>
                    <h3>Basic Info</h3>
                    <div className="basic-info">
                        <div className="info">
                            <label>Name<sup>*</sup></label>
                            <input
                                required
                                type="text"
                                placeholder="Name"
                                data-name="name"
                                onChange={this.onhandleChange}
                                value={hospital.name.value} />
                            {hospital.name.isError ? <div className="error">Please enter valid Name</div> : ''}

                            <label>About</label>
                            <input
                                type="text"
                                placeholder="About"
                                data-name="description"
                                onChange={this.onhandleChange}
                                value={hospital.description.value} />

                            <label>Email<sup>*</sup></label>
                            <input
                                required
                                type="email"
                                placeholder="Email"
                                data-name="email"
                                onChange={this.onhandleChange}
                                value={hospital.email.value} />
                            {hospital.email.isError ? <div className="error">Please enter valid Email</div> : ''}

                            <label>Mobile<sup>*</sup></label>
                            <input
                                required
                                type="text"
                                placeholder="Phone"
                                data-name="mobile"
                                onChange={this.onhandleChange}
                                value={hospital.mobile.value} />
                            {hospital.mobile.isError ? <div className="error">Please enter valid Mobile</div> : ''}

                        </div>
                        <div className="img">
                            {imageElement}
                            <div className="upload-img-btn">
                                UPLOAD
                                <input
                                    type="file"
                                    className="file-input"
                                    data-name="img"
                                    onChange={this.imageUploadHandler} />
                            </div>
                        </div>
                    </div>
                </form>

                <form>
                    <h3>Other Info</h3>

                    <div className="other-info">
                        <label>Experience Years</label>
                        <input
                            type="text"
                            placeholder="No of Beds"
                            data-name="no_of_beds"
                            onChange={this.onhandleChange}
                            value={hospital.no_of_beds.value} />

                        <label>Avg. Fees</label>
                        <input
                            type="text"
                            placeholder="Fees"
                            data-name="avg_fees"
                            onChange={this.onhandleChange}
                            value={hospital.avg_fees.value} />
                    </div>
                </form>

                <DoctorFeature
                    labelName="Treatments"
                    dataName="treatments"
                    doctor={hospital.treatments}
                    addFieldValues={this.addFieldValues}
                    mapFeatureValues={this.mapTreatment}
                />


                <div className="doctor-btn">
                    <span onClick={this.onClickSubmit}>SUBMIT</span>
                </div>

            </div>
        );
    }
}

AddHospital.propTypes = {
    hospital: PropTypes.object,
    actions: PropTypes.object
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(actions, dispatch)
    }
}

const mapStateToProps = (state) => {
    return {
        hospital: state.hospital
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddHospital);