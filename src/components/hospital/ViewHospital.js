import React from 'react';
import './hospital.scss';
import * as actions from '../../actions/hospitalActions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import queryString from 'query-string';
import { SERVER_URL } from '../../constants/app-config';

class ViewHospital extends React.Component {
    componentDidMount() {
        const id = queryString.parse(this.props.location.search).id;
        this.props.actions.get_hospital_by_id(id);
    }

    mapHospitalValues = (feature, index) => {
        return (
            <div key={index} className="feature-body">
                <div className="view-doctor-value" >{feature}</div>
            </div>
        );
    }

    render() {
        const hospital = this.props.hospital;
        let imageElement;
        if(hospital.hospital_pic_url.value){
            imageElement = <img src={SERVER_URL + '/' + hospital.hospital_pic_url.value} />
        }else{
            imageElement = <img src={require('../../assets/images/dummyHospital.png')}/>
        }
        return (
            <div className="doctor">
                <h2>View Hospital</h2>

                <form>
                    <h3>Basic Info</h3>
                    <div className="basic-info">
                        <div className="info">
                            <label>Name</label>
                            <div className="view-doctor-value">{hospital.name.value}</div>
                            <label>About</label>
                            <div className="view-doctor-value">{hospital.description.value}</div>
                            <label>Email</label>
                            <div className="view-doctor-value">{hospital.email.value}</div>
                            <label>Mobile</label>
                            <div className="view-doctor-value">{hospital.mobile.value}</div>
                        </div>
                        <div className="img">
                            {imageElement}
                            <div className="upload-img-btn">{hospital.name.value}</div>
                        </div>
                    </div>
                </form>

                <form>
                    <h3>Other Info</h3>
                    <div className="other-info">
                        <label>No of Beds</label>
                        <div className="view-doctor-value">{hospital.no_of_beds.value}</div>
                        <label>Avg. Fees</label>
                        <div className="view-doctor-value">{hospital.avg_fees.value}</div>
                    </div>
                </form>

                <form>
                    <div className="other">
                        <div className="field-heading">
                            <label>Treatments</label>
                        </div>
                        {hospital.treatments.value.map(this.mapHospitalValues)}
                    </div>
                </form>
    
            </div>
        );
    }
}

ViewHospital.propTypes = {
    hospital: PropTypes.object,
    location: PropTypes.object,
    actions: PropTypes.object
}

const mapStateToProps = (state) => {
    return {
        hospital: state.hospital
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(actions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ViewHospital);