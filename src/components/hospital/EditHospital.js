import React from 'react';
import './hospital.scss';
import * as actions from '../../actions/hospitalActions';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import queryString from 'query-string';
import DoctorFeature from '../doctor/DoctorFeature';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import { SERVER_URL } from '../../constants/app-config';

class EditHospital extends React.Component {

    constructor(props) {
        super(props);
        this.currentImage ='';
        this.img='';
    }

    componentDidMount() {
        const id = queryString.parse(this.props.location.search).id;
        this.props.actions.get_hospital_by_id(id);
    }

    onhandleChange = (event) => {
        const id = queryString.parse(this.props.location.search).id;

        const field = event.target.getAttribute('data-name');
        const value = event.target.value;

        this.props.actions.handle_edit_hospital_change({ id: id, field: field, value: value });
    }

    imageUploadHandler = (event) => {
        const files = event.currentTarget.files;
        const file = files[0];
        if(file){
            this.img = file;
            this.currentImage = URL.createObjectURL(file);
        }
        this.props.actions.image_upload_handler(file);
    }

    onhandleSubmit = () => {
        let obj = {};
        obj.id = this.props.hospital.id;
        for (var key in this.props.hospital) {
            if (this.props.hospital[key].edited) {
                if(key === 'hospital_pic_url'){
                    obj[key] = this.img
                }else{
                    obj[key] = this.props.hospital[key].value
                }
            }
        }
       
        // console.log(obj);
        this.props.actions.handle_edit_hospital_submit(obj);
    }

    addFieldValues = (event) => {
        const id = queryString.parse(this.props.location.search).id;

        const fieldName = event.currentTarget.getAttribute('data-name');
        const field = fieldName + '-' + this.props.hospital[fieldName].value.length;
        const value = '';

        this.props.actions.handle_edit_hospital_change({ id: id, field: field, value: value });
    }

    deleteFieldlValue = (event) => {
        const id = queryString.parse(this.props.location.search).id;

        const field = event.currentTarget.getAttribute('data-name');
        const value = undefined;

        this.props.actions.handle_edit_hospital_change({ id: id, field: field, value: value });
    }

    mapTreatment = (treatment, index) => {
        return (
            <div className="feature-body" key={index}>
                <input
                    type="text"
                    placeholder="Treatment"
                    data-name={"treatments-" + index}
                    onChange={this.onhandleChange}
                    value={treatment} />
                <div className="cancel" >
                    <span onClick={this.deleteFieldlValue} data-name={"treatments-" + index} >
                        <FontAwesomeIcon icon={faTimes} />
                    </span>
                </div>
            </div>
        );
    }

    render() {
        const hospital = this.props.hospital;
        let imageElement;
        if(this.currentImage){
            imageElement = <img src={this.currentImage}/>
        }else if(hospital.hospital_pic_url.value){
            imageElement = <img src={SERVER_URL + '/' + hospital.hospital_pic_url.value} />
        }else{
            imageElement = <img src={require('../../assets/images/dummyHospital.png')}/>
        }

        return (
            <div className="doctor">
                <h2>Edit Hospital</h2>

                <form>
                    <h3>Basic Info</h3>
                    <div className="basic-info">
                        <div className="info">

                            <label>Name</label>
                            <input
                                type="text"
                                placeholder="Name"
                                data-name='name'
                                value={hospital.name.value || ''}
                                onChange={this.onhandleChange}
                                readOnly />

                            <label>About</label>
                            <input
                                type="text"
                                placeholder="Description"
                                data-name='description'
                                value={hospital.description.value || ''}
                                onChange={this.onhandleChange} />

                            <label>Email</label>
                            <input
                                type="email"
                                placeholder="Email"
                                data-name='email'
                                value={hospital.email.value || ''}
                                onChange={this.onhandleChange}
                                readOnly />

                            <label>Mobile</label>
                            <input
                                type="text"
                                placeholder="Mobile"
                                data-name='mobile'
                                value={hospital.mobile.value || ''}
                                onChange={this.onhandleChange} 
                                readOnly/>
                        </div>

                        <div className="img">                        
                        {imageElement}
                            <div className="upload-img-btn">
                                EDIT
                                <input
                                    type="file"
                                    className="file-input"
                                    data-name="img"
                                    onChange={this.imageUploadHandler} />
                            </div>
                        </div>

                    </div>
                </form>

                <form>
                    <h3>Other Info</h3>
                    <div className="other-info">
                        <label>No of Beds</label>
                        <input
                            type="text"
                            placeholder="No of Beds"
                            data-name="no_of_beds"
                            onChange={this.onhandleChange}
                            value={hospital.no_of_beds.value || ''} />

                        <label>Avg. Fees</label>
                        <input
                            type="text"
                            placeholder="fees"
                            data-name="avg_fees"
                            onChange={this.onhandleChange}
                            value={hospital.avg_fees.value || ''} />

                    </div>

                </form>

                <DoctorFeature
                    labelName="Treatments"
                    dataName="treatments"
                    doctor={hospital.treatments}
                    addFieldValues={this.addFieldValues}
                    mapFeatureValues={this.mapTreatment} />


                <div className="doctor-btn">
                    <span onClick={this.onhandleSubmit}>SAVE</span>
                </div>

            </div>
        );
    }
}

EditHospital.propTypes = {
    hospital: PropTypes.object,
    location: PropTypes.object,
    actions: PropTypes.object
}

const mapStateToProps = (state) => {
    return {
        hospital: state.hospital
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(actions, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(EditHospital);