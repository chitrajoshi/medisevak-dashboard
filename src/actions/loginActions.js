import * as types from '../constants/action-types';
import { request } from "../api";

export const loginForm = (data) => {
    return async dispatch => {
        let response;
        try {
            response = await request("post", "/login", data, false);
            // console.log(response);

            if(response.status){
                dispatch(login_success(response.msg));
            }else{
                dispatch(login_failed(response.msg));
            }

        } catch (err) {
            // dispatch(reset_loading());
            // return dispatch(pop_toast({ status: false, msg: err.msg }));
        }
    }
}

const login_success = (data) => {
    return{ type: types.LOGIN_SUCCESS, data};
}

const login_failed = (data) => {
    return{ type: types.LOGIN_FAILED, data};
}

export const handle_login_form_change = (data) => {
    return {
        type: types.ON_CHANGE_LOGIN_FORM, data
    }
}


// fetch("http://localhost:4000/doctors",{
//     method: "POST", // *GET, POST, PUT, DELETE, etc.
//     mode: "cors",
//     cache: "no-cache",
//     headers: {
//         "Content-Type": "application/json",
//         "Accept": "application/json",
//         "Access-Control-Allow-Origin": "*"
//     },
//     body: JSON.stringify(data)
// })
//   .then(res => res.json())
//   .then(
//     (result) => {

//         console.log("result" + result);
//     //   this.setState({
//     //     isLoaded: true,
//     //     items: result.items
//     //   });
//     },
//     // Note: it's important to handle errors here
//     // instead of a catch() block so that we don't swallow
//     // exceptions from actual bugs in components.
//     (error) => {
//         console.log("error" + error);
//     //   this.setState({
//     //     isLoaded: true,
//     //     error
//     //   });
//     }
//   )


//     // return{ 
//     //     type: types.LOGIN_DASHBOARD,
//     //     loginData
//     // }
