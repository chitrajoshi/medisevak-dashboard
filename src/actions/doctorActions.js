import * as types from '../constants/action-types';
import { request } from '../api';
import { go_to_add_doctor, go_to_view_doctor, go_to_edit_doctor } from './routeActions/routeActions';
import { pop_toast } from './toastrActions';
import { set_loading, reset_loading } from './loaderActions';
import formValidation from '../validations/formValidation';

// DoctorList
export const get_all_doctors = () => {
    return async (dispatch) => {
        dispatch(set_loading("Loading"));
        let response;
        try {
            response = await request('get', '/doctors', '', false);
            // console.log(response);
            dispatch(reset_loading());
            if (response.status) {
                dispatch(pop_toast({ status: true, msg: `${response.data.length} doctors found` }));
                dispatch(get_doctors_successful(response.data));
            } else {
                //  dispatch(get_doctors_falied(response.msg));
                dispatch(pop_toast({ status: false, msg: `Server error : ${response.msg}` }));
            }
        } catch (err) {
            dispatch(pop_toast({ status: false, msg: `Network error : ${err}` }));
        }
    }
}

const get_doctors_successful = (data) => {
    return { type: types.GET_ALL_DOCTORS_SUCCESS, data };
}


// ViewDoctor
export const get_doctor_by_id = (id) => {
    return async (dispatch) => {
        dispatch(set_loading("Loading"));
        let response;
        try {
            const url = '/getDoctorById/?id=' + id
            response = await request('get', url, '', false);
            // console.log(response);
            dispatch(reset_loading());
            if (response.status) {
                dispatch(view_doctor_success(response.data));
                dispatch(pop_toast({ status: true, msg: `Success` }));
            } else {
                dispatch(pop_toast({ status: false, msg: `Server error : ${response.msg}` }));
            }
        } catch (err) {
            dispatch(pop_toast({ status: false, msg: `Network error : ${err}` }));
        }
    }
}

const view_doctor_success = (data) => {
    return {
        type: types.VIEW_DOCTOR_SUCCESS, data
    }
}


// AddDoctor : Form change
export const handle_doctor_form_change = (data) => {
    return {
        type: types.ON_CHANGE_DOCTOR_FORM,
        data
    }
}

// Add Doctor
export const handle_doctor_form_submit = (data,img) => {
    return async dispatch => {

        if (formValidation("name", data.name.value)) {
            if (formValidation("email", data.email.value)) {
                if (formValidation("mobile", data.mobile.value)) {

                    const doctor = data;

                    const newDoctor = {};
                    newDoctor.name = doctor.name.value;
                    newDoctor.description = doctor.description.value;
                    newDoctor.email = doctor.email.value;
                    newDoctor.mobile = doctor.mobile.value;
                    newDoctor.experience_years = doctor.experience_years.value;
                    newDoctor.avg_fees = doctor.avg_fees.value;
                    newDoctor.profile_pic_url = img;

                    newDoctor.treatments = doctor.treatments.value;
                    newDoctor.specializations = doctor.specializations.value;
                    newDoctor.qualifications = doctor.qualifications.value;
                    newDoctor.work_experiences = doctor.work_experiences.value;
                    newDoctor.awards = doctor.awards.value;
                    newDoctor.hospital_list = doctor.hospital_list.value;

                    // console.log(newDoctor);

                    dispatch(set_loading("Loading"));
                    let response;
                    try {
                        response = await request('post', '/addDoctor', newDoctor, true);
                        // console.log(response);
                        dispatch(reset_loading());
                        if (response.status) {
                            dispatch(pop_toast({ status: true, msg: response.msg }));
                            dispatch(add_doctor_reset());
                        } else {
                            dispatch(pop_toast({ status: false, msg: `Server error : ${response.msg}` }));
                        }
                    } catch (err) {
                        dispatch(pop_toast({ status: false, msg: `Network error : ${err}` }));
                    }

                } else {
                    dispatch(add_doctor_failure({ field: "mobile" }));
                }
            } else {
                dispatch(add_doctor_failure({ field: "email" }));
            }
        } else {
            dispatch(add_doctor_failure({ field: "name" }));
        }


    }
}

const add_doctor_reset = () => {
    return {
        type: types.ADD_DOCTOR_RESET
    }
}

const add_doctor_failure = (data) => {
    return {
        type: types.ADD_DOCTOR_FAILURE,
        data
    }
}


// Delete Doctor

export const on_handle_delete_doctor = (id) => {
    return async (dispatch) => {
        dispatch(set_loading("Loading"));
        let response;
        try {
            const url = '/deleteDoctorById/?id=' + id
            response = await request('get', url, '', false)
            // console.log(response);
            dispatch(reset_loading());
            if (response.status) {
                dispatch(pop_toast({ status: true, msg: response.msg }));
                dispatch(delete_doctor_success(id));
            } else {
                dispatch(pop_toast({ status: false, msg: `Server error : ${response.msg}` }));
            }
        } catch (err) {
            dispatch(pop_toast({ status: false, msg: `Network error : ${err}` }));
        }
    }
}

const delete_doctor_success = (id) => {
    return {
        type: types.DELETE_DOCTOR_SUCCESS,
        id
    }
}



// Edit Doctor : Form Change

export const handle_edit_doctor_change = (data) => {
    return {
        type: types.ON_CHANGE_EDIT_DOCTOR_FORM,
        data
    }
}

export const handle_edit_doctor_submit = (data) => {
    return async (dispatch) => {
        dispatch(set_loading("Loading"));
        
        let response;
        try {
            response = await request('put', '/editDoctorById', data, true);
            // console.log(response);
            dispatch(reset_loading());
            if (response.status) {
                dispatch(pop_toast({ status: true, msg: response.msg }));
                dispatch(edit_doctor_success(response));
            } else {
                dispatch(pop_toast({ status: false, msg: `Server error : ${response.msg}` }));
            }
        }
        catch (err) {
            dispatch(pop_toast({ status: false, msg: `Network error : ${err}` }));
        }
    }
}

const edit_doctor_success = (data) => {
    return {
        type: types.EDIT_DOCTOR_SUCCESS,
        data
    }
}

// Search DoctorList

export const on_handle_Search = (searchString) => {
    return {
        type: types.SEARCH_DOCTORS,
        data: searchString
    }
}

//Image Upload

export const image_upload_handler = (cover_img) => {
    const validationObject = formValidation("img", cover_img);
    return dispatch => {
        const image = cover_img;
        if (!validationObject.status) {
            return dispatch(pop_toast({ status: false, msg: validationObject.msg }));
        }
        return dispatch(onUploadImage(image));
    }
}

const onUploadImage = (img) => {
    return { type: types.UPLOAD_DOCTOR_IMAGE, img };
}


// Route actions

export const on_handle_add_doctor = () => {
    return dispatch => {
        dispatch(go_to_add_doctor());
        dispatch(add_doctor_reset());
    }
}

export const on_handle_view_doctor = (id) => {
    return dispatch => {
        dispatch(go_to_view_doctor(id));
    }
}

export const on_handle_edit_doctor = (id) => {
    return dispatch => {
        dispatch(go_to_edit_doctor(id));
    }
}