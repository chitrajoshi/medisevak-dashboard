import * as types from '../constants/action-types';
import generateRandomNumber from '../utils/generateRandomNumber';

export const pop_toast = (data) => {
    return(dispatch) => {
        const random = Math.random();
        const demoId = generateRandomNumber(random);
        setTimeout( () => {
            dispatch(remove_toast({id: demoId}));
        }, 1000);

        dispatch(pop_toast_success({id: demoId, ...data}))
    }
}

export const pop_toast_success = (data) => {
    return{
        type : types.POP_TOAST,
        data
    }
}

export const remove_toast = (data) => {
    return{
        type : types.REMOVE_TOAST,
        data
    }
}