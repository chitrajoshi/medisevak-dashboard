import {push} from 'connected-react-router';
import * as types from '../../constants/action-types';

//doctors
export const go_to_doctors = (data) => {
    return dispatch => {
        dispatch(push('/doctors'));
        dispatch(update_menu(data));
    };
}

export const go_to_add_doctor = () => {
    return dispatch => dispatch(push('/addDoctor'));
}

export const go_to_view_doctor = (id) => {
    return dispatch => dispatch(push('/viewDoctor/?id=' + id));
}

export const go_to_edit_doctor = (id) => {
    return dispatch => dispatch(push('/editdoctor/?id=' + id));
}


//hospitals

export const go_to_hospitals = (data) => {
    return dispatch => {
        dispatch(push('/hospitals'));
        dispatch(update_menu(data));
    };
}

export const go_to_add_hospital = () => {
    return dispatch => dispatch(push('/addHospital'));
}

export const go_to_view_hospital = (id) => {
    return dispatch => dispatch(push('/viewHospital/?id=' + id));
}

export const go_to_edit_hospital = (id) => {
    return dispatch => dispatch(push('/editHospital/?id=' + id));
}


//treatments

export const go_to_treatments = (data) =>{
    return dispatch => {
        dispatch(push('/treatments'));
        dispatch(update_menu(data));
    };
}


//patients

export const go_to_patients = (data) => {
    return dispatch => {
        dispatch(push('/patients'));
        dispatch(update_menu(data));
    };
}

export const go_to_add_patient = () => {
    return dispatch => dispatch(push('/addPatient'));
}

export const go_to_view_patient = (id) => {
    return dispatch => dispatch(push('/viewPatient/?id=' + id));
}

export const go_to_edit_patient = (id) => {
    return dispatch => dispatch(push('/editPatient/?id=' + id));
}

//sidebar

const update_menu = (data) => {
    return{
        type : types.UPDATE_SIDEBAR,
        data
    }
}