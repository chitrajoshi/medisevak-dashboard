import * as types from '../constants/action-types';
import { request } from '../api';
import { go_to_add_patient, go_to_view_patient, go_to_edit_patient } from './routeActions/routeActions';
import { pop_toast } from './toastrActions';
import { set_loading, reset_loading } from './loaderActions';
import formValidation from '../validations/formValidation';

// PatientList
export const get_all_patients = () => {
    return async (dispatch) => {
        dispatch(set_loading("Loading"));
        let response;
        try {
            response = await request('get', '/patients', '', false);
            // console.log(response);
            dispatch(reset_loading());
            if (response.status) {
                dispatch(pop_toast({ status: true, msg: `${response.data.length} doctors found` }));
                dispatch(get_patients_successful(response.data));
            } else {
                //  dispatch(get_doctors_falied(response.msg));
                dispatch(pop_toast({ status: false, msg: `Server error : ${response.msg}` }));
            }
        } catch (err) {
            dispatch(pop_toast({ status: false, msg: `Network error : ${err}` }));
        }
    }
}

const get_patients_successful = (data) => {
    return { type: types.GET_ALL_PATIENTS_SUCCESS, data };
}


// ViewPatient
export const get_patient_by_id = (id) => {
    return async (dispatch) => {
        dispatch(set_loading("Loading"));
        let response;
        try {
            const url = '/getPatientById/?id=' + id
            response = await request('get', url, '', false);
            // console.log(response);
            dispatch(reset_loading());
            if (response.status) {
                dispatch(view_patient_success(response.data));
                dispatch(pop_toast({ status: true, msg: `Success` }));
            } else {
                dispatch(pop_toast({ status: false, msg: `Server error : ${response.msg}` }));
            }
        } catch (err) {
            dispatch(pop_toast({ status: false, msg: `Network error : ${err}` }));
        }
    }
}

const view_patient_success = (data) => {
    return {
        type: types.VIEW_PATIENT_SUCCESS, data
    }
}


// AddPatient : Form change
export const handle_patient_form_change = (data) => {
    return {
        type: types.ON_CHANGE_PATIENT_FORM,
        data
    }
}

// Add Patient
export const handle_patient_form_submit = (data,img) => {
    return async dispatch => {

        if (formValidation("name", data.name.value)) {
            if (formValidation("email", data.email.value)) {
                if (formValidation("mobile", data.mobile.value)) {

                    const patient = data;

                    const newPatient = {};
                    newPatient.name = patient.name.value;
                    newPatient.email = patient.email.value;
                    newPatient.mobile = patient.mobile.value;
                    newPatient.gender = patient.gender.value;
                    newPatient.dob = patient.dob.value;
                    newPatient.patient_pic_url = img;

                   
                    // console.log(newDoctor);

                    dispatch(set_loading("Loading"));
                    let response;
                    try {
                        response = await request('post', '/addPatient', newPatient, true);
                        // console.log(response);
                        dispatch(reset_loading());
                        if (response.status) {
                            dispatch(pop_toast({ status: true, msg: response.msg }));
                            dispatch(add_patient_reset());
                        } else {
                            dispatch(pop_toast({ status: false, msg: `Server error : ${response.msg}` }));
                        }
                    } catch (err) {
                        dispatch(pop_toast({ status: false, msg: `Network error : ${err}` }));
                    }

                } else {
                    dispatch(add_patient_failure({ field: "mobile" }));
                }
            } else {
                dispatch(add_patient_failure({ field: "email" }));
            }
        } else {
            dispatch(add_patient_failure({ field: "name" }));
        }


    }
}

const add_patient_reset = () => {
    return {
        type: types.ADD_PATIENT_RESET
    }
}

const add_patient_failure = (data) => {
    return {
        type: types.ADD_PATIENT_FAILURE,
        data
    }
}


// Delete Patient
export const on_handle_delete_patient = (id) => {
    return async (dispatch) => {
        dispatch(set_loading("Loading"));
        let response;
        try {
            const url = '/deletePatientById/?id=' + id
            response = await request('get', url, '', false)
            // console.log(response);
            dispatch(reset_loading());
            if (response.status) {
                dispatch(pop_toast({ status: true, msg: response.msg }));
                dispatch(delete_patient_success(id));
            } else {
                dispatch(pop_toast({ status: false, msg: `Server error : ${response.msg}` }));
            }
        } catch (err) {
            dispatch(pop_toast({ status: false, msg: `Network error : ${err}` }));
        }
    }
}

const delete_patient_success = (id) => {
    return {
        type: types.DELETE_PATIENT_SUCCESS,
        id
    }
}


// Edit Patient : Form Change
export const handle_edit_patient_change = (data) => {
    return {
        type: types.ON_CHANGE_EDIT_PATIENT_FORM,
        data
    }
}

export const handle_edit_patient_submit = (data) => {
    return async (dispatch) => {
        dispatch(set_loading("Loading"));
        
        let response;
        try {
            response = await request('put', '/editPatientById', data, true);
            // console.log(response);
            dispatch(reset_loading());
            if (response.status) {
                dispatch(pop_toast({ status: true, msg: response.msg }));
                dispatch(edit_patient_success(response));
            } else {
                dispatch(pop_toast({ status: false, msg: `Server error : ${response.msg}` }));
            }
        }
        catch (err) {
            dispatch(pop_toast({ status: false, msg: `Network error : ${err}` }));
        }
    }
}

const edit_patient_success = (data) => {
    return {
        type: types.EDIT_PATIENT_SUCCESS,
        data
    }
}


// Search PatientList
export const on_handle_Search = (searchString) => {
    return {
        type: types.SEARCH_PATIENTS,
        data: searchString
    }
}


//Image Upload
export const image_upload_handler = (cover_img) => {
    const validationObject = formValidation("img", cover_img);
    return dispatch => {
        const image = cover_img;
        if (!validationObject.status) {
            return dispatch(pop_toast({ status: false, msg: validationObject.msg }));
        }
        return dispatch(onUploadImage(image));
    }
}

const onUploadImage = (img) => {
    return { type: types.UPLOAD_PATIENT_IMAGE, img };
}


// Route actions
export const on_handle_add_patient = () => {
    return dispatch => {
        dispatch(go_to_add_patient());
        dispatch(add_patient_reset());
    }
}

export const on_handle_view_patient = (id) => {
    return dispatch => {
        dispatch(go_to_view_patient(id));
    }
}

export const on_handle_edit_patient = (id) => {
    return dispatch => {
        dispatch(go_to_edit_patient(id));
    }
}