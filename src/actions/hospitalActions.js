import * as types from '../constants/action-types';
import { request } from '../api';
import { go_to_add_hospital, go_to_view_hospital, go_to_edit_hospital } from './routeActions/routeActions';
import { pop_toast } from './toastrActions';
import { set_loading, reset_loading } from './loaderActions';
import formValidation from '../validations/formValidation';

// HospitalList
export const get_all_hospitals = () => {
    return async (dispatch) => {
        dispatch(set_loading("Loading"));
        let response;
        try {
            response = await request('get', '/hospitals', '', false);
            // console.log(response);
            dispatch(reset_loading());
            if (response.status) {
                dispatch(pop_toast({ status: true, msg: `${response.data.length} doctors found` }));
                dispatch(get_hospitals_successful(response.data));
            } else {
                //  dispatch(get_doctors_falied(response.msg));
                dispatch(pop_toast({ status: false, msg: `Server error : ${response.msg}` }));
            }
        } catch (err) {
            dispatch(pop_toast({ status: false, msg: `Network error : ${err}` }));
        }
    }
}

const get_hospitals_successful = (data) => {
    return { type: types.GET_ALL_HOSPITALS_SUCCESS, data };
}

// View Hospital
export const get_hospital_by_id = (id) => {
    return async (dispatch) => {
        dispatch(set_loading("Loading"));
        let response;
        try {
            const url = '/getHospitalById/?id=' + id
            response = await request('get', url, '', false);
            // console.log(response);
            dispatch(reset_loading());
            if (response.status) {
                dispatch(view_hospital_success(response.data));
                dispatch(pop_toast({ status: true, msg: `Success` }));
            } else {
                dispatch(pop_toast({ status: false, msg: `Server error : ${response.msg}` }));
            }
        } catch (err) {
            dispatch(pop_toast({ status: false, msg: `Network error : ${err}` }));
        }
    }
}

const view_hospital_success = (data) => {
    return {
        type: types.VIEW_HOSPITAL_SUCCESS, data
    }
}

// Add Hospital : Form change
export const handle_hospital_form_change = (data) => {
    return {
        type: types.ON_CHANGE_HOSPITAL_FORM,
        data
    }
}

// Add Hospital
export const handle_hospital_form_submit = (data,img) => {
    return async dispatch => {

        if (formValidation("name", data.name.value)) {
            if (formValidation("email", data.email.value)) {
                if (formValidation("mobile", data.mobile.value)) {

                    const hospital = data;

                    const newHospital = {};
                    newHospital.name = hospital.name.value;
                    newHospital.description = hospital.description.value;
                    newHospital.email = hospital.email.value;
                    newHospital.mobile = hospital.mobile.value;
                    newHospital.no_of_beds = hospital.no_of_beds.value;
                    newHospital.avg_fees = hospital.avg_fees.value;
                    newHospital.hospital_pic_url = img;

                    newHospital.treatments = hospital.treatments.value;

                    // console.log(newHospital);

                    dispatch(set_loading("Loading"));
                    let response;
                    try {
                        response = await request('post', '/addHospital', newHospital, true);
                        // console.log(response);
                        dispatch(reset_loading());
                        if (response.status) {
                            dispatch(pop_toast({ status: true, msg: response.msg }));
                            dispatch(add_hospital_reset());
                        } else {
                            dispatch(pop_toast({ status: false, msg: `Server error : ${response.msg}` }));
                        }
                    } catch (err) {
                        dispatch(pop_toast({ status: false, msg: `Network error : ${err}` }));
                    }

                } else {
                    dispatch(add_hospital_failure({ field: "mobile" }));
                }
            } else {
                dispatch(add_hospital_failure({ field: "email" }));
            }
        } else {
            dispatch(add_hospital_failure({ field: "name" }));
        }
    }
}

const add_hospital_reset = () => {
    return {
        type: types.ADD_HOSPITAL_RESET
    }
}

const add_hospital_failure = (data) => {
    return {
        type: types.ADD_HOSPITAL_FAILURE,
        data
    }
}



// Delete Hospital

export const on_handle_delete_hospital = (id) => {
    return async (dispatch) => {
        dispatch(set_loading("Loading"));
        let response;
        try {
            const url = '/deleteHospitalById/?id=' + id
            response = await request('get', url, '', false)
            // console.log(response);
            dispatch(reset_loading());
            if (response.status) {
                dispatch(pop_toast({ status: true, msg: response.msg }));
                dispatch(delete_hospital_success(id));
            } else {
                dispatch(pop_toast({ status: false, msg: `Server error : ${response.msg}` }));
            }
        } catch (err) {
            dispatch(pop_toast({ status: false, msg: `Network error : ${err}` }));
        }
    }
}

const delete_hospital_success = (id) => {
    return {
        type: types.DELETE_HOSPITAL_SUCCESS,
        id
    }
}

// Edit Hospital : Form Change

export const handle_edit_hospital_change = (data) => {
    return {
        type: types.ON_CHANGE_EDIT_HOSPITAL_FORM,
        data
    }
}

export const handle_edit_hospital_submit = (data) => {
    return async (dispatch) => {
        dispatch(set_loading("Loading"));
        
        let response;
        try {
            response = await request('put', '/editHospitalById', data, true);
            // console.log(response);
            dispatch(reset_loading());
            if (response.status) {
                dispatch(pop_toast({ status: true, msg: response.msg }));
                dispatch(edit_hospital_success(response));
            } else {
                dispatch(pop_toast({ status: false, msg: `Server error : ${response.msg}` }));
            }
        }
        catch (err) {
            dispatch(pop_toast({ status: false, msg: `Network error : ${err}` }));
        }
    }
}

const edit_hospital_success = (data) => {
    return {
        type: types.EDIT_HOSPITAL_SUCCESS,
        data
    }
}

// Search HospitalList

export const on_handle_Search = (searchString) => {
    return {
        type: types.SEARCH_HOSPITALS,
        data: searchString
    }
}

//Image Upload

export const image_upload_handler = (cover_img) => {
    const validationObject = formValidation("img", cover_img);
    return dispatch => {
        const image = cover_img;
        if (!validationObject.status) {
            return dispatch(pop_toast({ status: false, msg: validationObject.msg }));
        }
        return dispatch(onUploadImage(image));
    }
}

const onUploadImage = (img) => {
    return { type: types.UPLOAD_HOSPITAL_IMAGE, img };
}

// Route actions

export const on_handle_add_hospital = () => {
    return dispatch => {
        dispatch(go_to_add_hospital());
        dispatch(add_hospital_reset());
    }
}

export const on_handle_view_hospital = (id) => {
    return dispatch => {
        dispatch(go_to_view_hospital(id));
    }
}

export const on_handle_edit_hospital = (id) => {
    return dispatch => {
        dispatch(go_to_edit_hospital(id));
    }
}