import * as types from '../constants/action-types';

export const set_loading = (msg) => {
    return dispatch => {
        setTimeout(() => {
            return dispatch({ type: types.SET_LOADING, msg })
        }, 100);
    }
}

export const reset_loading = () => {
    return dispatch => {
        setTimeout(() => {
            return dispatch({ type: types.RESET_LOADING });
        }, 100)
    }
}