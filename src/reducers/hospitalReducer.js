import * as types from '../constants/action-types';
import initialState from './initial-state';

const hospitalReducer = (state = initialState.hospitalModel(), actions) => {
    
    switch(actions.type){
        case types.VIEW_HOSPITAL_SUCCESS: {

            const newState = Object.assign({}, state);
            newState.id = actions.data._id;

            const newNameObj = Object.assign({}, newState.name);
            newNameObj.value = actions.data.name;
            newState.name = newNameObj;

            const newDescObj = Object.assign({}, newState.description);
            newDescObj.value = actions.data.description;
            newState.description = newDescObj;

            const newEmailObj = Object.assign({}, newState.email);
            newEmailObj.value = actions.data.email;
            newState.email = newEmailObj;

            const newMobileObj = Object.assign({}, newState.mobile);
            newMobileObj.value = actions.data.mobile;
            newState.mobile = newMobileObj;

            const newNoOfBedsObj = Object.assign({}, newState.no_of_beds);
            newNoOfBedsObj.value = actions.data.no_of_beds;
            newState.no_of_beds = newNoOfBedsObj;

            const newAvgFeesObj = Object.assign({}, newState.avg_fees);
            newAvgFeesObj.value = actions.data.avg_fees;
            newState.avg_fees = newAvgFeesObj;

            const newProfilePicUrlObj = Object.assign({}, newState.hospital_pic_url);
            newProfilePicUrlObj.value = actions.data.hospital_pic_url;
            newState.hospital_pic_url = newProfilePicUrlObj;

            const newTreatmentsObj = Object.assign({}, newState.treatments);
            newTreatmentsObj.value = [...actions.data.treatments];
            newState.treatments = newTreatmentsObj;

            // console.log(newState);
            return newState;

        }
        case types.ON_CHANGE_HOSPITAL_FORM: {

            const field = actions.data.field;
            const value = actions.data.value;

            // console.log(field, value);

            const newState = Object.assign({}, state);

            if (field.includes('-')) {
                const [fieldName, StringIndex] = field.split('-');
                // console.log(fieldName, index);

                const index = Number.parseInt(StringIndex);
                const newFieldObject = Object.assign({}, newState[fieldName]);
                // console.log(newFieldObject);

                let newFieldArray = [...newFieldObject.value]
                if (value !== undefined) {
                    newFieldArray[index] = value;
                } else {
                    newFieldArray = [...newFieldArray.slice(0, index), ...newFieldArray.slice(index + 1)]
                }

                newFieldObject.value = newFieldArray;
                newState[fieldName] = newFieldObject;

            } else {

                const newFieldObject = Object.assign({}, newState[field]);
                newFieldObject.value = value;
                newState[field] = newFieldObject;
            }

            // console.log(newState);
            return newState;
        }
        case types.ADD_HOSPITAL_RESET: {

            const newState = initialState.hospitalModel();
            return newState;

        }
        case types.ADD_HOSPITAL_FAILURE: {

            const field = actions.data.field;
            let newState = Object.assign({}, state);
            let fieldObj = Object.assign({}, newState[field]);
            fieldObj.isError = true;
            newState[field] = fieldObj;

            return newState;
        }
        case types.ON_CHANGE_EDIT_HOSPITAL_FORM: {
            // console.log(actions.data);

            const id = actions.data.id;
            const field = actions.data.field;
            const value = actions.data.value;

            let newState = Object.assign({}, state);
            newState.id = id;

            if (field.includes('-')) {
                const [fieldName, StringIndex] = field.split('-');
                // console.log(fieldName, index);

                const index = Number.parseInt(StringIndex);
                const newFieldObject = Object.assign({}, newState[fieldName]);
                // console.log(newFieldObject);

                let newFieldArray = [...newFieldObject.value]
                if (value !== undefined) {

                    newFieldArray[index] = value;
                } else {
                    newFieldArray = [...newFieldArray.slice(0, index), ...newFieldArray.slice(index + 1)]
                }

                newFieldObject.value = newFieldArray;
                newFieldObject.edited = true;
                newState[fieldName] = newFieldObject;
                // console.log(newState);

            } else {
                const newFieldObject = Object.assign({}, newState[field]);
                newFieldObject.value = value;
                newFieldObject.edited = true;
                newState[field] = newFieldObject;
            }

            return newState;
        }
        case types.EDIT_HOSPITAL_SUCCESS: {
            return state;
        }
        case types.UPLOAD_HOSPITAL_IMAGE: {

            const newState = Object.assign({}, state);
            let hospitalFieldObj = Object.assign({}, newState['hospital_pic_url']);
            hospitalFieldObj.edited = true;
            newState['hospital_pic_url'] = hospitalFieldObj;
            return newState;
        }
        default: {
            return state;
        }
    }
}

export default hospitalReducer;