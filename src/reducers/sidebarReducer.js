import * as types from '../constants/action-types';

const initialState = {selected: "doctor"};
const SidebarReducer = (state= initialState, actions) => {
    switch(actions.type){
        case types.UPDATE_SIDEBAR : {
            const updatedMenu = actions.data;            
            const newState = Object.assign({}, state);
            newState.selected = updatedMenu;
            // console.log(newState);
            return newState;
        }
        default : {
            return state;
        }
    }
}

export default SidebarReducer;