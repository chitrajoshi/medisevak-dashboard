import * as types from '../constants/action-types';

const initialState = {
    is_loading: false,
    msg: "Loading"
  }

const pageLoaderReducer = (state = initialState, actions) => {
    switch(actions.type){
        case types.SET_LOADING: {
            const newState = Object.assign({}, state);
            newState.is_loading = true;
            newState.msg = actions.msg
            return newState;
          }
          case types.RESET_LOADING: {
            const newState = Object.assign({}, state);
            newState.is_loading = false;
            newState.msg = "Loading";
            return newState;
          }
          default: {
            return state
          }
    }
}

export default pageLoaderReducer;