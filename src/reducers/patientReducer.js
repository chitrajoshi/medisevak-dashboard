import * as types from '../constants/action-types';
import initialState from './initial-state';

const patientReducer = (state = initialState.patientModel(), actions) => {
    switch (actions.type) {
        case types.VIEW_PATIENT_SUCCESS: {

            const newState = Object.assign({}, state);
            newState.id = actions.data._id;

            const newNameObj = Object.assign({}, newState.name);
            newNameObj.value = actions.data.name;
            newState.name = newNameObj;

            const newEmailObj = Object.assign({}, newState.email);
            newEmailObj.value = actions.data.email;
            newState.email = newEmailObj;

            const newMobileObj = Object.assign({}, newState.mobile);
            newMobileObj.value = actions.data.mobile;
            newState.mobile = newMobileObj;

            const newGenderObj = Object.assign({}, newState.gender);
            newGenderObj.value = actions.data.gender;
            newState.gender = newGenderObj;

            const newDOBObj = Object.assign({}, newState.dob);
            newDOBObj.value = actions.data.dob;
            newState.dob = newDOBObj;

            const newPatientPicUrlObj = Object.assign({}, newState.patient_pic_url);
            newPatientPicUrlObj.value = actions.data.patient_pic_url;
            newState.patient_pic_url = newPatientPicUrlObj;

            // console.log(newState);
            return newState;
        }
        case types.ON_CHANGE_PATIENT_FORM: {

            const field = actions.data.field;
            const value = actions.data.value;

            // console.log(field, value);

            const newState = Object.assign({}, state);

            if (field.includes('-')) {
                const [fieldName, StringIndex] = field.split('-');
                // console.log(fieldName, index);

                const index = Number.parseInt(StringIndex);
                const newFieldObject = Object.assign({}, newState[fieldName]);
                // console.log(newFieldObject);

                let newFieldArray = [...newFieldObject.value]
                if (value !== undefined) {
                    newFieldArray[index] = value;
                } else {
                    newFieldArray = [...newFieldArray.slice(0, index), ...newFieldArray.slice(index + 1)]
                }

                newFieldObject.value = newFieldArray;
                newState[fieldName] = newFieldObject;

            } else {

                const newFieldObject = Object.assign({}, newState[field]);
                newFieldObject.value = value;
                newState[field] = newFieldObject;
            }

            // console.log(newState);
            return newState;
        }
        case types.ADD_PATIENT_RESET: {

            const newState = initialState.patientModel();
            return newState;

        }
        case types.ADD_PATIENT_FAILURE: {

            const field = actions.data.field;

            let newState = Object.assign({}, state);
            let fieldObj = Object.assign({}, newState[field]);
            fieldObj.isError = true;
            newState[field] = fieldObj;

            return newState;
        }
        case types.ON_CHANGE_EDIT_PATIENT_FORM: {
            // console.log(actions.data);

            const id = actions.data.id;
            const field = actions.data.field;
            const value = actions.data.value;

            let newState = Object.assign({}, state);
            newState.id = id;

            if (field.includes('-')) {
                const [fieldName, StringIndex] = field.split('-');
                // console.log(fieldName, index);

                const index = Number.parseInt(StringIndex);
                const newFieldObject = Object.assign({}, newState[fieldName]);
                // console.log(newFieldObject);

                let newFieldArray = [...newFieldObject.value]
                if (value !== undefined) {

                    newFieldArray[index] = value;
                } else {
                    newFieldArray = [...newFieldArray.slice(0, index), ...newFieldArray.slice(index + 1)]
                }

                newFieldObject.value = newFieldArray;
                newFieldObject.edited = true;
                newState[fieldName] = newFieldObject;
                // console.log(newState);

            } else {
                const newFieldObject = Object.assign({}, newState[field]);
                newFieldObject.value = value;
                newFieldObject.edited = true;
                newState[field] = newFieldObject;
            }

            return newState;
        }
        case types.EDIT_PATIENT_SUCCESS: {
            return state;
        }
        case types.UPLOAD_PATIENT_IMAGE: {

            const newState = Object.assign({}, state);
            let patientFieldObj = Object.assign({}, newState['patient_pic_url']);
            patientFieldObj.edited = true;
            newState['patient_pic_url'] = patientFieldObj;
            return newState;
        }
        default: {
            return state;
        }
    }
}

export default patientReducer;
