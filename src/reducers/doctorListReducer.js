import * as types from '../constants/action-types';

const initialState = {doctors: [], filteredDoctors: []};

const doctorListReducer = (state = initialState, actions) => {
    switch(actions.type){
        case types.GET_ALL_DOCTORS_SUCCESS: {

            const newState = Object.assign({}, state);
            newState.doctors = [...actions.data];
            newState.filteredDoctors = [...actions.data];
            return newState;
        }
        case types.DELETE_DOCTOR_SUCCESS: {

            const id = actions.id;
            const newState = Object.assign({}, state);

            let newFilteredDoctor = [...newState.filteredDoctors];
            const deleteIndex1 = newFilteredDoctor.findIndex((item) => item._id === id);
            newFilteredDoctor = [...newFilteredDoctor.slice(0, deleteIndex1), ...newFilteredDoctor.slice(deleteIndex1+1)];
            newState.filteredDoctors = [...newFilteredDoctor];

            let newDoctor = [...newState.doctors];
            const deleteIndex = newDoctor.findIndex((item) => item._id === id);
            newDoctor = [...newDoctor.slice(0, deleteIndex), ...newDoctor.slice(deleteIndex+1)];
            newState.doctors = [...newDoctor];

            return newState;   
        }
        case types.SEARCH_DOCTORS: {

            const searchString = actions.data;
            
            let newState = Object.assign({}, state);
            let newDoctor = [...newState.doctors];

            const newArray = newDoctor.filter((doctor) => {
                if(doctor.name.includes(searchString)){
                    return true;
                }
            });

            newState.filteredDoctors = [...newArray];
            return newState;
        }
        default: {
            return state;
        }
    }
}

export default doctorListReducer;