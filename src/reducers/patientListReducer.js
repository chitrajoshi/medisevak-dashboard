import * as types from '../constants/action-types';

const initialState = {patients: [], filteredPatients: []};

const patientListReducer = (state = initialState, actions) => {
    switch(actions.type){
        case types.GET_ALL_PATIENTS_SUCCESS: {

            const newState = Object.assign({}, state);
            newState.patients = [...actions.data];
            newState.filteredPatients = [...actions.data];
            return newState;
        }
        case types.DELETE_PATIENT_SUCCESS: {

            const id = actions.id;
            const newState = Object.assign({}, state);

            let newFilteredPatient = [...newState.filteredPatients];
            const deleteIndex1 = newFilteredPatient.findIndex((item) => item._id === id);
            newFilteredPatient = [...newFilteredPatient.slice(0, deleteIndex1), ...newFilteredPatient.slice(deleteIndex1+1)];
            newState.filteredPatients = [...newFilteredPatient];

            let newPatient = [...newState.patients];
            const deleteIndex = newPatient.findIndex((item) => item._id === id);
            newPatient = [...newPatient.slice(0, deleteIndex), ...newPatient.slice(deleteIndex+1)];
            newState.patients = [...newPatient];

            return newState;   
        }
        case types.SEARCH_PATIENTS: {

            const searchString = actions.data;
            
            let newState = Object.assign({}, state);
            let newPatient = [...newState.patients];

            const newArray = newPatient.filter((patient) => {
                if(patient.name.includes(searchString)){
                    return true;
                }
            });

            newState.filteredPatients = [...newArray];
            return newState;
        }
        default: {
            return state;
        }
    }
}

export default patientListReducer;