const initialState = {
    doctorModel : _doctor_model,
    hospitalModel : _hospital_model,
    patientModel : _patient_model
};

function _doctor_model() {
    return {
        id: undefined,
        name: {
            value: '',
            edited: false,
            isError: false
        },
        description: {
            value: '',
            edited: false
        },
        email: {
            value: '',
            edited: false,
            isError: false
        },
        mobile: {
            value: '',
            edited: false,
            isError: false
        },
        experience_years: {
            value: '',
            edited: false
        },
        avg_fees: {
            value: '',
            edited: false
        },
        profile_pic_url: {
            value: '',
            edited: false
        },
        treatments: {
            value: [],
            edited: false
        },
        specializations: {
            value: [],
            edited: false
        },
        qualifications: {
            value: [],
            edited: false
        },
        work_experiences: {
            value: [],
            edited: false
        },
        awards: {
            value: [],
            edited: false
        },
        hospital_list: {
            value: [],
            edited: false
        }
    }
}

function _hospital_model() {
    return {
        id: undefined,
        name: {
            value: '',
            edited: false,
            isError: false
        },
        description: {
            value: '',
            edited: false
        },
        email: {
            value: '',
            edited: false,
            isError: false
        },
        mobile: {
            value: '',
            edited: false,
            isError: false
        },
        no_of_beds: {
            value: '',
            edited: false
        },
        avg_fees: {
            value: '',
            edited: false
        },
        hospital_pic_url: {
            value: '',
            edited: false
        },
        treatments: {
            value: [],
            edited: false
        }
    }
}

function _patient_model() {
    return {
        id: undefined,
        name: {
            value: '',
            edited: false,
            isError: false
        },
        email: {
            value: '',
            edited: false,
            isError: false
        },
        mobile: {
            value: '',
            edited: false,
            isError: false
        },
        gender: {
            value: '',
            edited: false
        },
        dob: {
            value: '',
            edited: false
        },
        patient_pic_url: {
            value: '',
            edited: false
        }
    }
}

export default initialState;
