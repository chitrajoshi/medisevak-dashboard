import * as types from '../constants/action-types';

const initialState = {mobile : "", password : "", msg : "", loginStatus: false};

const loginReducer = (state = initialState , actions) => {
    switch(actions.type){
        case types.ON_CHANGE_LOGIN_FORM : {
            const field = actions.data.field;
            const value = actions.data.value;
            const newState = Object.assign({}, state);

            newState[field] =  value;
       
            return newState;
        }
        case types.LOGIN_SUCCESS: {
            const newState = Object.assign({}, state);
            newState.msg = actions.data;
            newState.loginStatus = true;
            return newState;
        }
        case types.LOGIN_FAILED: {
            const newState = Object.assign({}, state);
            newState.msg = actions.data;
            return newState;
        }
        default: {
            return state;
            }
    }
}

export default loginReducer;