import * as types from '../constants/action-types';
import initialState from './initial-state';

const doctorReducer = (state = initialState.doctorModel(), actions) => {

    switch (actions.type) {
        case types.VIEW_DOCTOR_SUCCESS: {

            const newState = Object.assign({}, state);
            newState.id = actions.data._id;

            const newNameObj = Object.assign({}, newState.name);
            newNameObj.value = actions.data.name;
            newState.name = newNameObj;

            const newDescObj = Object.assign({}, newState.description);
            newDescObj.value = actions.data.description;
            newState.description = newDescObj;

            const newEmailObj = Object.assign({}, newState.email);
            newEmailObj.value = actions.data.email;
            newState.email = newEmailObj;

            const newMobileObj = Object.assign({}, newState.mobile);
            newMobileObj.value = actions.data.mobile;
            newState.mobile = newMobileObj;

            const newExpYrsObj = Object.assign({}, newState.experience_years);
            newExpYrsObj.value = actions.data.experience_years;
            newState.experience_years = newExpYrsObj;

            const newAvgFeesObj = Object.assign({}, newState.avg_fees);
            newAvgFeesObj.value = actions.data.avg_fees;
            newState.avg_fees = newAvgFeesObj;

            const newProfilePicUrlObj = Object.assign({}, newState.profile_pic_url);
            newProfilePicUrlObj.value = actions.data.profile_pic_url;
            newState.profile_pic_url = newProfilePicUrlObj;

            const newTreatmentsObj = Object.assign({}, newState.treatments);
            newTreatmentsObj.value = [...actions.data.treatments];
            newState.treatments = newTreatmentsObj;

            const newSpecializationsObj = Object.assign({}, newState.specializations);
            newSpecializationsObj.value = [...actions.data.specializations];
            newState.specializations = newSpecializationsObj;

            const newQualificationsObj = Object.assign({}, newState.qualifications);
            newQualificationsObj.value = [...actions.data.qualifications];
            newState.qualifications = newQualificationsObj;

            const newWorkExpObj = Object.assign({}, newState.work_experiences);
            newWorkExpObj.value = [...actions.data.work_experiences];
            newState.work_experiences = newWorkExpObj;

            const newAwardsObj = Object.assign({}, newState.awards);
            newAwardsObj.value = [...actions.data.awards];
            newState.awards = newAwardsObj;

            const newHospitalListObj = Object.assign({}, newState.hospital_list);
            newHospitalListObj.value = [...actions.data.hospital_list];
            newState.hospital_list = newHospitalListObj;

            // console.log(newState);
            return newState;
        }
        case types.ON_CHANGE_DOCTOR_FORM: {

            const field = actions.data.field;
            const value = actions.data.value;

            // console.log(field, value);

            const newState = Object.assign({}, state);

            if (field.includes('-')) {
                const [fieldName, StringIndex] = field.split('-');
                // console.log(fieldName, index);

                const index = Number.parseInt(StringIndex);
                const newFieldObject = Object.assign({}, newState[fieldName]);
                // console.log(newFieldObject);

                let newFieldArray = [...newFieldObject.value]
                if (value !== undefined) {
                    newFieldArray[index] = value;
                } else {
                    newFieldArray = [...newFieldArray.slice(0, index), ...newFieldArray.slice(index + 1)]
                }

                newFieldObject.value = newFieldArray;
                newState[fieldName] = newFieldObject;

            } else {

                const newFieldObject = Object.assign({}, newState[field]);
                newFieldObject.value = value;
                newState[field] = newFieldObject;
            }

            // console.log(newState);
            return newState;
        }
        case types.ADD_DOCTOR_RESET: {

            const newState = initialState.doctorModel();
            return newState;

        }
        case types.ADD_DOCTOR_FAILURE: {

            const field = actions.data.field;

            let newState = Object.assign({}, state);
            let fieldObj = Object.assign({}, newState[field]);
            fieldObj.isError = true;
            newState[field] = fieldObj;

            return newState;
        }
        case types.ON_CHANGE_EDIT_DOCTOR_FORM: {
            // console.log(actions.data);

            const id = actions.data.id;
            const field = actions.data.field;
            const value = actions.data.value;

            let newState = Object.assign({}, state);
            newState.id = id;

            if (field.includes('-')) {
                const [fieldName, StringIndex] = field.split('-');
                // console.log(fieldName, index);

                const index = Number.parseInt(StringIndex);
                const newFieldObject = Object.assign({}, newState[fieldName]);
                // console.log(newFieldObject);

                let newFieldArray = [...newFieldObject.value]
                if (value !== undefined) {

                    newFieldArray[index] = value;
                } else {
                    newFieldArray = [...newFieldArray.slice(0, index), ...newFieldArray.slice(index + 1)]
                }

                newFieldObject.value = newFieldArray;
                newFieldObject.edited = true;
                newState[fieldName] = newFieldObject;
                // console.log(newState);

            } else {
                const newFieldObject = Object.assign({}, newState[field]);
                newFieldObject.value = value;
                newFieldObject.edited = true;
                newState[field] = newFieldObject;
            }

            return newState;
        }
        case types.EDIT_DOCTOR_SUCCESS: {
            return state;
        }
        case types.UPLOAD_DOCTOR_IMAGE: {

            const newState = Object.assign({}, state);
            let doctorFieldObj = Object.assign({}, newState['profile_pic_url']);
            doctorFieldObj.edited = true;
            newState['profile_pic_url'] = doctorFieldObj;
            return newState;
        }
        default: {
            return state;
        }
    }
}

export default doctorReducer;