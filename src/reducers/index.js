// Set up your root reducer here...
 import { combineReducers } from 'redux';
 import login from './loginReducer';
 import doctorList from './doctorListReducer';
 import doctor from './doctorReducer';
 import toasts from './toastrReducer';
 import pageLoader from './pageLoaderReducer';
 import sideBar from './sidebarReducer';
 import hospitalList from './hospitalListReducer';
 import hospital from './hospitalReducer';
 import patientList from './patientListReducer';
 import patient from './patientReducer';

 const rootReducer = combineReducers({
    login,
    doctorList,
    doctor,
    toasts,
    pageLoader,
    sideBar,
    hospitalList,
    hospital,
    patientList,
    patient
 });

 export default rootReducer;