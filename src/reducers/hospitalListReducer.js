import * as types from '../constants/action-types';

const initialState = {hospitals: [], filteredHospitals: []};

const hospitalListReducer = (state = initialState, actions) => {
    switch(actions.type){
        case types.GET_ALL_HOSPITALS_SUCCESS: {

            const newState = Object.assign({}, state);
            newState.hospitals = [...actions.data];
            newState.filteredHospitals = [...actions.data];
            return newState;
            
        }
        case types.SEARCH_HOSPITALS: {

            const searchString = actions.data;
            
            let newState = Object.assign({}, state);
            let newHospital = [...newState.hospitals];

            const newArray = newHospital.filter((hospital) => {
                if(hospital.name.includes(searchString)){
                    return true;
                }
            });

            newState.filteredHospitals = [...newArray];
            return newState;
        }
        case types.DELETE_HOSPITAL_SUCCESS: {

            const id = actions.id;
            const newState = Object.assign({}, state);

            let newFilteredHospital = [...newState.filteredHospitals];
            const deleteIndex1 = newFilteredHospital.findIndex((item) => item._id === id);
            newFilteredHospital = [...newFilteredHospital.slice(0, deleteIndex1), ...newFilteredHospital.slice(deleteIndex1+1)];
            newState.filteredHospitals = [...newFilteredHospital];

            let newHospital = [...newState.hospitals];
            const deleteIndex = newHospital.findIndex((item) => item._id === id);
            newHospital = [...newHospital.slice(0, deleteIndex), ...newHospital.slice(deleteIndex+1)];
            newState.hospitals = [...newHospital];

            return newState;   
        }
        default : {
            return state;
        }
    }
}

export default hospitalListReducer;
