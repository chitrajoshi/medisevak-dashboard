import * as types from '../constants/action-types';

const initialState = [];

const toastrReducer = (state = initialState, actions) => {
    switch(actions.type){
        case types.POP_TOAST: {
            let newState = [...state];
            const newToast = {
                id : actions.data.id,
                msg : actions.data.msg,
                status : actions.data.status
            }
            newState = [newToast];
            return newState;
        }
        case types.REMOVE_TOAST : {
            let newState = [...state];
            const toasts = _filterArray(newState);
            const elemPosition = _findPositionById(toasts, actions.data.id);
            newState = [...toasts.slice(0, elemPosition - 1), ...toasts.slice(elemPosition + 1, toasts.length)]
            return newState;
        }
        default : {
            return state;
        }
    }
}

const _filterArray = (arr) => {
    return arr.sort((el1, el2) => {
      return el1.id > el2.id;
    });
  }
  
  const _findPositionById = (arr, id) => {
    let position = 0;
    for (let elem of arr) {
      if (elem.id == id) {
        return position;
      }
      position++;
    }
  }

export default toastrReducer;